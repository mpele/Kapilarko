package rs.mpele.capilaritty;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import com.fazecast.jSerialComm.SerialPort;

public class Balance {
	private SerialPort comPort;

	//TODO !!!!!!! izbrisati ovu promenjivu i srediti ono šta ona povlači
	private static boolean test = false; 


	private Double tmpBrojac = 3.; // TODO !!!! pomocna promenjiva - obrisati

	private File mFolder;

	private Double mLastValue;

	private boolean debugBalance = false; 

	@SuppressWarnings("static-access")
	public static void main(final String[] args) throws InterruptedException {
		Balance balance = new Balance();
		balance.initialize();
		balance.setWorkFolder(new File("."));

		Thread.currentThread().sleep(1000);

		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());
		System.out.println(balance.readMass());


		//System.out.println(balance.readData("SI   -   2.7773 g  "));
	}

	public void setWorkFolder(File file) {
		mFolder = file;
	}

	public void initialize() {

		if(comPort!= null && comPort.isOpen()){
			return;
		}

		if(!test){
			SerialPort[] ports = SerialPort.getCommPorts();

			for(SerialPort port: ports){
				System.out.println(port.getDescriptivePortName());
				if(port.getDescriptivePortName().contains("RADWAG")){				
					comPort = port;
					System.out.println(comPort.getDescriptivePortName());
				}

			}

			if(comPort == null){
				JOptionPane.showMessageDialog(null,
						"Balance is not connected !!!",
						"Inane error",
						JOptionPane.ERROR_MESSAGE);
			}

			comPort.openPort();
			if(!comPort.isOpen()) {
				JOptionPane.showMessageDialog(null,
						"Port for balance is not open!",
						"Inane error",
						JOptionPane.ERROR_MESSAGE);
				System.out.println("!!!!!!!!!!!!! port is not opened");
			}
			comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);

			sendCommand("C1\r\n");

			clearBuffer();
		}
	}

	public double readMass() {
		if(test){
			tmpBrojac = tmpBrojac*(0.9+.2*Math.random());
			appendToFile(tmpBrojac);
			return tmpBrojac;
		}

		if(!comPort.isOpen()) {
			JOptionPane.showMessageDialog(null,
					"Port for balance is not open!",
					"Inane error",
					JOptionPane.ERROR_MESSAGE);
			System.out.println("!!!!!!!!!!!!! port is not opened");
		}

		clearBuffer();

		//sendCommand("SI\r\n");
		//System.out.println("Salje komandu");


		boolean haveResult = false;
		Double value = null;
		String data = "";
		byte[] readBuffer = new byte[21]; // number of bytes that are expected 
		comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 4000, 0);
		int counter = 0;

		while(haveResult == false){ // TODO !!!!! set timer
			int numRead = comPort.readBytes(readBuffer, readBuffer.length);
			if(debugBalance == true) debugBalance(readBuffer);

			if(numRead>0){
				data = data + new String(readBuffer);				
			}
			//System.out.println("Read " + numRead + " bytes: " + data);

			String[] tmp = data.split("\n\r");
			if(tmp != null && tmp[counter].startsWith("SI")){
				value = readData(tmp[counter]);
				haveResult = true;
			}
			else{
				counter++;
			}
		}

		mLastValue = value; 
		appendToFile(value);

		return value;
	}

	private void sendCommand(String command) {
		byte[] arduinoKomanda = (command).getBytes();
		comPort.writeBytes(arduinoKomanda , arduinoKomanda.length);
	}

	private void appendToFile(Double value) {
		try
		{
			String filename= mFolder.toString()+"/mass.txt";
			FileWriter fw = new FileWriter(filename,true); //the true will append the new data
			fw.write(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.S").format(new Date()) + ";" + String.valueOf(value)+"\n");
			fw.close();
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}		
	}

	public void close(){
		if(!test){
			comPort.closePort();			
		}
	}

	public void clearBuffer(){
		byte[] readBuffer2 = new byte[1000]; // number of bytes that are expected				
		comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 10, 0);
		while(comPort.bytesAvailable()>0){
			comPort.readBytes(readBuffer2, readBuffer2.length);
			if(debugBalance == true) debugBalance(readBuffer2);
			//System.out.println("Clear buffer: " + numRead2 + "-"+new String(readBuffer2));
		}
	}


	/**
	 * Returns mass that is max 1s old
	 * @return
	 */
	public Double getLastMass(){
		return mLastValue;
	}

	private Double readData(String data){
		Double value = Double.valueOf(data.substring(7, 15)); // masa bi trebalo da se nalazi izmedju 7 i 15 karaktera (brojanje od 1)
		if(data.substring(5, 6).equals("-")) 
			value = - value;
		return value;
	}

	private void debugBalance(byte[] readBuffer) {
		try
		{
			String filename= mFolder.toString()+"/massDebug.txt";
			FileWriter fw = new FileWriter(filename,true); //the true will append the new data
			fw.write(new String(readBuffer));
			fw.close();
		}
		catch(IOException ioe)
		{
			System.err.println("IOException: " + ioe.getMessage());
		}		
	}

	public void setDebug(boolean b) {
		debugBalance = true;		
	}
}
