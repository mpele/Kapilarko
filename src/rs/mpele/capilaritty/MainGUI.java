package rs.mpele.capilaritty;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.SystemColor;

import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

import net.miginfocom.swing.MigLayout;
import rs.mpele.capilaritty.exportToMp4.ExportToMP4;
import rs.mpele.capilaritty.selection.PictureSelectionDialog;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;

import marvin.io.MarvinImageIO;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.util.prefs.Preferences;
import java.awt.event.ActionEvent;

import javax.swing.JButton;

import java.awt.Frame;

public class MainGUI {

	private File mWorkFolder;

	private JFrame frmCapilaritty;
	private WickingSetPanel wickingSetPanel;
	private JMenuBar menuBar;
	private JMenu mnSettings;
	private JMenuItem mntmGetOldSettings;
	private JMenuItem mntmSetWorkingFolder;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMeasurament;
	private JMenuItem mntmDefineSection;

	private GraphPanel graphPanel;
	private JMenuItem mntmRecalculate;
	private JMenuItem mntmRecalculate2;
	private TimeTrigerPanel timeTrigerPanel;
	private JButton btnStartBalnceRecording;

	private Balance mBalance;

	Preferences prefs = Preferences.userRoot().node(getClass().getName());
	private JMenuItem mntmLivePreview;
	private JButton btnDebug;
	private JMenuItem mntmRemovePointsOut;
	private JMenuItem mntmThresholdSettings;
	private JMenuItem mntmExportCurrentImages;
	private JMenuItem mntmCreateMpFrom;
	private JMenuItem mntmCreateMpFrom_1;
	private JMenuItem mntmExportAllMeasurementsWicking;
	private JMenu mnExportData;
	private JMenu mnExportToXlsx;
	private JMenuItem mntmExportMass;
	private JMenuItem mntmExportWickingMass;
	private JMenuItem mntmExportForNeuroph;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frmCapilaritty.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGUI() {
		frmCapilaritty = new JFrame();
		frmCapilaritty.setExtendedState(Frame.MAXIMIZED_BOTH);
		frmCapilaritty.setTitle("Capilaritty");
		frmCapilaritty.setBounds(100, 100, 1100, 800);
		frmCapilaritty.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCapilaritty.getContentPane().setLayout(new MigLayout("", "[grow][grow]", "[grow][grow][][]"));

		mWorkFolder = new File(loadLastUsedFoleder());

		graphPanel = new GraphPanel();
		wickingSetPanel = new WickingSetPanel(mWorkFolder, graphPanel);
		mBalance = new Balance();
		mBalance.setWorkFolder(mWorkFolder);

		frmCapilaritty.getContentPane().add(wickingSetPanel, "flowx,cell 0 0");

		graphPanel.setMeasurements(wickingSetPanel.getWickingSet().getMeasurements());
		graphPanel.loadMass(mWorkFolder);
		frmCapilaritty.getContentPane().add(graphPanel, "cell 1 0,grow");

		timeTrigerPanel = new TimeTrigerPanel(wickingSetPanel, mWorkFolder);
		frmCapilaritty.getContentPane().add(timeTrigerPanel, "cell 0 1,grow");

		btnStartBalnceRecording = new JButton("Start balance recording");
		btnStartBalnceRecording.addActionListener(new ActionListener() {
			boolean activeRecording = false;
			Thread thread;

			public void actionPerformed(ActionEvent arg0) {
				if (!activeRecording) {
					mBalance.initialize();
					btnStartBalnceRecording.setText("Stop balance recording");

					Runnable runnable = new Runnable() {
						int counter = 100;

						@Override
						public void run() {
							while (true) {
								double mass = mBalance.readMass();
								graphPanel.addMassData(mass);
								if(++counter >= 60){
									System.out.println("Mass: " + mass);
								}
								counter = counter % 60;

								try {
									Thread.sleep(1000);
								} catch (InterruptedException e) {
									throw new IllegalStateException(e);
								}
							}
						}
					};

					thread = new Thread(runnable);
					thread.start();
				} else {
					thread.stop();
					mBalance.close();
					btnStartBalnceRecording.setText("Start balance recording");
				}
				activeRecording = !activeRecording;

			}
		});
		frmCapilaritty.getContentPane().add(btnStartBalnceRecording, "flowx,cell 0 2");

		btnDebug = new JButton("Debug");
		btnDebug.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mBalance.setDebug(true);
			}
		});
		frmCapilaritty.getContentPane().add(btnDebug, "cell 0 2");

		menuBar = new JMenuBar();
		frmCapilaritty.setJMenuBar(menuBar);

		mnNewMenu = new JMenu("Measuraments");
		menuBar.add(mnNewMenu);

		mntmNewMeasurament = new JMenuItem("New measurament");
		mntmNewMeasurament.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					mWorkFolder = chooser.getSelectedFile();
					try {
						Files.copy(Paths.get("section.txt"), Paths.get(mWorkFolder.toPath() + "\\section.txt"));
						Files.copy(Paths.get("calibration.txt"), Paths.get(mWorkFolder.toPath() + "\\calibration.txt"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					frmCapilaritty.setTitle(mWorkFolder.getPath() + "\\");

					mBalance.setWorkFolder(mWorkFolder);
					timeTrigerPanel.setFolder(mWorkFolder);
					wickingSetPanel.setFolder(mWorkFolder);
					wickingSetPanel.updatePanel();
					graphPanel.setMeasurements(wickingSetPanel.getWickingSet().getMeasurements());
					graphPanel.clearMassData();

					setLastUsedFolder(mWorkFolder);
				} else {
					System.out.println("No Selection ");
				}
			}
		});
		mnNewMenu.add(mntmNewMeasurament);

		mntmRecalculate = new JMenuItem("Recalculate"); 
		mntmRecalculate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				recalculate(false);
			}
		});
		mntmRecalculate.setToolTipText("Manualy setted values for wickings are not deleted");
		mnNewMenu.add(mntmRecalculate);


		mntmRecalculate2 = new JMenuItem("Recalculate all"); 
		mntmRecalculate2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int dialogResult = JOptionPane.YES_OPTION;
				if(wickingSetPanel.getWickingSet().getMeasurements().isThereAnyManualySettedWicking()){
					dialogResult = JOptionPane.showOptionDialog(null,
							"There are manualy setted values and this operation is going to owerwrite it?\n"
									+ "                                Continue?",
									"Warning", JOptionPane.YES_NO_OPTION,
									JOptionPane.QUESTION_MESSAGE, null, 
									new String[] {"Yes", "No"}, "No");
				}

				if(dialogResult == JOptionPane.YES_OPTION){
					try {
						Files.delete(Paths.get(mWorkFolder + "/measurements.txt"));
						wickingSetPanel.getWickingSet().loadMeasurementsData();       // TODO !!!!!!!! Ovo je neleogicno - brise se fajl pa se trazi njegovo ucitavanje
						// TODO !!!!!! Da resetuje i measuraments na WickingSet -
						// Sada ostaju repovi
					} catch (IOException e) {
						System.out.println(e.getMessage());
					}
					recalculate(true);
					wickingSetPanel.updadeDataFromWickingSet();
					wickingSetPanel.updatePanel();
				}
			}
		});
		mnNewMenu.add(mntmRecalculate2);


		mnSettings = new JMenu("Settings");
		menuBar.add(mnSettings);

		mntmGetOldSettings = new JMenuItem("Get old settings");
		mntmGetOldSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				//
				// disable the "All files" option.
				//
				chooser.setAcceptAllFileFilterUsed(false);
				//
				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
					try {
						Files.copy(Paths.get(chooser.getSelectedFile().toPath() + "\\section.txt"),
								Paths.get(mWorkFolder.toPath() + "\\section.txt"));
						Files.copy(Paths.get(chooser.getSelectedFile().toPath() + "\\calibration.txt"),
								Paths.get(mWorkFolder.toPath() + "\\calibration.txt"));
					} catch (IOException e) {
						JOptionPane.showMessageDialog(null,
								"Problem with getting old settings !!!",
								"Inane error",
								JOptionPane.ERROR_MESSAGE);
						e.printStackTrace();
					}
					System.out.println(Paths.get(chooser.getSelectedFile().toPath() + "\\calibration.txt"));
					System.out.println(Paths.get(mWorkFolder.toPath() + "\\calibration.txt"));
				} else {
					System.out.println("No Selection ");
				}
			}
		});

		mntmSetWorkingFolder = new JMenuItem("Set working folder");
		mntmSetWorkingFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File(loadLastUsedFoleder()));

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					System.out.println("getCurrentDirectory(): " + mWorkFolder);
					System.out.println("getSelectedFile() : " + chooser.getSelectedFile());

					mWorkFolder = chooser.getSelectedFile();
					setLastUsedFolder(mWorkFolder);
					frmCapilaritty.setTitle(mWorkFolder.getPath() + "\\");

					timeTrigerPanel.setFolder(mWorkFolder);
					mBalance.setWorkFolder(mWorkFolder);
					wickingSetPanel.setFolder(mWorkFolder);
					wickingSetPanel.updatePanel();
					graphPanel.setMeasurements(wickingSetPanel.getWickingSet().getMeasurements());
					graphPanel.loadMass(mWorkFolder);
				} else {
					System.out.println("No Selection ");
				}
			}
		});

		mntmLivePreview = new JMenuItem("Live Preview");
		mntmLivePreview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Webcam webcam = timeTrigerPanel.getWebcam();
				WebcamPanel panel = new WebcamPanel(webcam);
				panel.setFPSDisplayed(true);
				panel.setDisplayDebugInfo(true);
				panel.setImageSizeDisplayed(true);
				panel.setPainter(new PlotProfileWebcamPanelPainter());

				JFrame window = new JFrame("Test webcam panel");
				window.getContentPane().add(panel);
				window.setResizable(true);
				window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				window.pack();
				window.setVisible(true);
			}
		});
		mnSettings.add(mntmLivePreview);
		mnSettings.add(mntmSetWorkingFolder);

		JMenuItem mntmCurrentSettingsAs = new JMenuItem("Current settings as default");
		mntmCurrentSettingsAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Files.copy(Paths.get(mWorkFolder.toPath() + "\\section.txt"), Paths.get(".\\section.txt"),
							StandardCopyOption.REPLACE_EXISTING);
					Files.copy(Paths.get(mWorkFolder.toPath() + "\\calibration.txt"), Paths.get(".\\calibration.txt"),
							StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		mnSettings.add(mntmCurrentSettingsAs);
		mnSettings.add(mntmGetOldSettings);

		mntmDefineSection = new JMenuItem("Define section");
		mntmDefineSection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println(wickingSetPanel.getCurrentPictureFileName());
				PictureSelectionDialog dialog = new PictureSelectionDialog(wickingSetPanel.getCurrentPictureFileName(),
						wickingSetPanel.getWickingSet().getSection(), wickingSetPanel.getWickingSet().getCalibration());
				Section res = dialog.showDialog();
				if(res == null){
					return;
				}
				System.out.println(res);
				recalculate(false); 
			}

		});
		mnSettings.add(mntmDefineSection);

		mntmThresholdSettings = new JMenuItem("Threshold settings");
		mntmThresholdSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//System.out.println(wickingSetPanel.getCurrentPhotoSection().getThresholdSettings());
				HistogramTuningDialog dialog = new HistogramTuningDialog(wickingSetPanel.getCurrentPhotoSection(), wickingSetPanel.getCurrentSlideTimeSeconds());
				ThresholdSettings res = dialog.showDialog();
				if(res == null){
					return;
				}
				try {
					res.save();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				System.out.println("New ThresholdSettings "+res);

				recalculate_UpdateThresholdSettings(res);
			}
		});
		mnSettings.add(mntmThresholdSettings);

		JMenu mnData = new JMenu("Data");
		menuBar.add(mnData);

		JMenuItem mntmAddMeasurementOn = new JMenuItem("Add measurement on graph");
		mntmAddMeasurementOn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);

				if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					System.out.println("getCurrentDirectory(): " + chooser.getCurrentDirectory());
					System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
					Measurements measurement2 = new Measurements(chooser.getSelectedFile());
					measurement2.load();
					graphPanel.setAdditionalMeasurement(measurement2);
				} else {
					System.out.println("No Selection ");
				}
			}
		});
		mnData.add(mntmAddMeasurementOn);

		JMenuItem mntmOpenFolderWith = new JMenuItem("Open folder with pictures");
		mntmOpenFolderWith.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Desktop.getDesktop().open(mWorkFolder);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		mnData.add(mntmOpenFolderWith);

		mntmExportCurrentImages = new JMenuItem("Export current images");
		mntmExportCurrentImages.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MarvinImageIO.saveImage(wickingSetPanel.getCurrentPhotoSection().getOriginalPhoto(),wickingSetPanel.getCurrentPictureFileName()+"section.png");
				MarvinImageIO.saveImage(wickingSetPanel.getCurrentPhotoSection().getPreparedPhoto(),wickingSetPanel.getCurrentPictureFileName()+"processed.png");
			}
		});
		mnData.add(mntmExportCurrentImages);

		mnExportData = new JMenu("Export data");
		mnData.add(mnExportData);

		mnExportToXlsx = new JMenu("Export to xlsx all data measurements from folder");
		mnExportData.add(mnExportToXlsx);

		mntmExportAllMeasurementsWicking = new JMenuItem("Wicking");
		mntmExportAllMeasurementsWicking.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				processExportToExcel(ExportType.WICKING);
			}
		});
		mnExportToXlsx.add(mntmExportAllMeasurementsWicking);
		
		mntmExportMass = new JMenuItem("Mass");
		mntmExportMass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				processExportToExcel(ExportType.MASS);				
			}
		});
		mnExportToXlsx.add(mntmExportMass);
		
		mntmExportWickingMass = new JMenuItem("Wicking and mass");
		mntmExportWickingMass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				processExportToExcel(ExportType.WICKIN_MASS);
			}
		});
		mnExportToXlsx.add(mntmExportWickingMass);

		JMenuItem mntmExportToCsv = new JMenuItem("Export to csv");
		mnExportData.add(mntmExportToCsv);
		mntmExportToCsv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					wickingSetPanel.getWickingSet().getMeasurements().export2csv();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		mntmRemovePointsOut = new JMenuItem("Remove points out of interest");
		mntmRemovePointsOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graphPanel.removePointsOutOfInterest();
			}
		});
		
		mntmExportForNeuroph = new JMenuItem("Export for Neuroph learning");
		mntmExportForNeuroph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				wickingSetPanel.getWickingSet().ExportForNeuroph(150, 255);
			}
		});
		mnData.add(mntmExportForNeuroph);
		
		mntmCreateMpFrom = new JMenuItem("Create MP4 from images");
		mntmCreateMpFrom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ExportToMP4.createMP4(mWorkFolder.getAbsolutePath());
			}
		});

		mnData.add(mntmCreateMpFrom);

//		mntmCreateMpFrom_1 = new JMenuItem("Create MP4 from sections");
//		mntmCreateMpFrom_1.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				wickingSetPanel.getWickingSet().createMP4();
//			}
//		});
//		mnData.add(mntmCreateMpFrom_1);
//		mnData.add(mntmRemovePointsOut);

		frmCapilaritty.setTitle(mWorkFolder.getPath());
	}

	private String loadLastUsedFoleder() {
		String defaultFolder = ".";
		String lastFolder = prefs.get("LAST_USED_FOLDER", defaultFolder);
		if (!new File(lastFolder).exists()) {
			System.out.println("The last used folder "+lastFolder+" doesn't exist. Uses "+defaultFolder);
			JOptionPane.showMessageDialog(null,
					"The last used folder "+lastFolder+" doesn't exist. Uses "+defaultFolder,
					"Inane error",
					JOptionPane.ERROR_MESSAGE);
			lastFolder = defaultFolder;
		}
		System.out.println("Last used folder:" + lastFolder);
		return lastFolder;
	}

	private void setLastUsedFolder(File pWorkFolder) {
		System.out.println("Setting last used folder: "+ pWorkFolder);
		prefs.put("LAST_USED_FOLDER", pWorkFolder.getAbsolutePath());
	}

	private void recalculate(boolean processAllFiles) {
		final JDialog loading = new JDialog(this.frmCapilaritty);
		JPanel p1 = new JPanel(new BorderLayout());
		p1.setBackground(SystemColor.info);
		p1.setSize(new Dimension(400, 200));
		p1.setPreferredSize(new Dimension(400, 200));
		p1.setMinimumSize(new Dimension(400, 200));
		p1.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

		JLabel lblPleaseWait = new JLabel("Please wait ...");
		lblPleaseWait.setHorizontalTextPosition(SwingConstants.CENTER);
		lblPleaseWait.setHorizontalAlignment(SwingConstants.CENTER);
		lblPleaseWait.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPleaseWait.setForeground(SystemColor.desktop);
		lblPleaseWait.setAlignmentX(Component.CENTER_ALIGNMENT);
		lblPleaseWait.setBounds(new Rectangle(30, 30, 30, 30));
		p1.add(lblPleaseWait, BorderLayout.CENTER);

		loading.getContentPane().add(p1);

		loading.pack();
		loading.setLocationRelativeTo(this.frmCapilaritty);
		loading.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		loading.setModal(true);

		SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
			@Override
			protected String doInBackground() throws InterruptedException {
				try {
					wickingSetPanel.getWickingSet().processFiles(processAllFiles);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}

			@Override
			protected void done() {
				loading.dispose();
				wickingSetPanel.updatePanel();
				Beep.end();
			}
		};
		worker.execute();
		loading.setVisible(true);
		try {
			worker.get();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		graphPanel.setMeasurements(wickingSetPanel.getWickingSet().getMeasurements());
		wickingSetPanel.updateUI();
	}


	private void recalculate_UpdateThresholdSettings(ThresholdSettings res) {
		wickingSetPanel.getWickingSet().setThresholdSettings(res);
		wickingSetPanel.getWickingSet().updateThresholdSettings();
		wickingSetPanel.updatePanel();
		graphPanel.setMeasurements(wickingSetPanel.getWickingSet().getMeasurements());
		wickingSetPanel.updateUI();
	}

	private void processExportToExcel(ExportType exportType) {
		String defaultFolder = ".";
		String lastFolder = prefs.get("LAST_USED_FOLDER_WITH_MEASURAMENTS", defaultFolder);
		if (!new File(lastFolder).exists()) {
			System.out.println("The last used folder with measurements "+lastFolder+" doesn't exist. Uses "+defaultFolder);
			JOptionPane.showMessageDialog(null,
					"The last used folder with measuremnts "+lastFolder+" doesn't exist. Uses "+defaultFolder,
					"Inane error",
					JOptionPane.ERROR_MESSAGE);
			lastFolder = defaultFolder;
		}
		System.out.println("Last used folder:" + lastFolder);

		JFileChooser chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File(lastFolder));

		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);

		if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			System.out.println("getSelectedFile() : " + chooser.getSelectedFile());

			mWorkFolder = chooser.getSelectedFile();
			prefs.put("LAST_USED_FOLDER_WITH_MEASURAMENTS", mWorkFolder.getAbsolutePath());
		} else {
			System.out.println("No Selection ");
			return;
		}

		String file = "Measurements.xlsx";
		ExportToExcel exportToExcel;
		try {
			exportToExcel = new ExportToExcel(exportType, mWorkFolder.getAbsolutePath());
			exportToExcel.close(file);

			Desktop desktop = Desktop.getDesktop();
			desktop.open(new File(file));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
