package rs.mpele.capilaritty;
import javax.sound.sampled.*;

public class Beep {

	public static float SAMPLE_RATE = 8000f;

	public static void tone(int hz, int msecs) 
			throws LineUnavailableException 
	{
		tone(hz, msecs, 1.0);
	}

	public static void tone(int hz, int msecs, double vol)
			throws LineUnavailableException 
	{
		byte[] buf = new byte[1];
		AudioFormat af = 
				new AudioFormat(
						SAMPLE_RATE, // sampleRate
						8,           // sampleSizeInBits
						1,           // channels
						true,        // signed
						false);      // bigEndian
		SourceDataLine sdl = AudioSystem.getSourceDataLine(af);
		sdl.open(af);
		sdl.start();
		for (int i=0; i < msecs*8; i++) {
			double angle = i / (SAMPLE_RATE / hz) * 2.0 * Math.PI;
			buf[0] = (byte)(Math.sin(angle) * 127.0 * vol);
			sdl.write(buf,0,1);
		}
		sdl.drain();
		sdl.stop();
		sdl.close();
	}

	public static void end(){
		Runnable r = new Runnable() {
			public void run() {
				try {
					Beep.tone(1000,100);
					Thread.sleep(500);
					Beep.tone(1000,100);
					Thread.sleep(500);
					Beep.tone(1000,1000);
				} catch (LineUnavailableException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};

		new Thread(r).start();
	}

	public static void longBeep(){
		Runnable r = new Runnable() {
			public void run() {
				try {
					Beep.tone(1000,500);
				} catch (LineUnavailableException e) {
					e.printStackTrace();
				}
			}
		};

		new Thread(r).start();
	}

	public static void main(String[] args) throws Exception {

		Beep.end();
		System.out.println("Gotovo");

		//		Beep.tone(1000,100);
		//		Thread.sleep(500);
		//		Beep.tone(1000,100);
		//		Thread.sleep(500);
		//		Beep.tone(100,1000);
		//		Thread.sleep(1000);
		//		Beep.tone(5000,100);
		//		Thread.sleep(1000);
		//		Beep.tone(400,500);
		//		Thread.sleep(1000);
		//		Beep.tone(400,500, 0.2);


		for(int i = 0; i <= 3 ; i++){
			for(int j = 0; j <= 3 && j <= i; j++){
				for(int k = 0; k <= 1 && k <= j; k++){
					
System.out.println(i + " " + j + " "+k );
				}
			}
		}

	}
}