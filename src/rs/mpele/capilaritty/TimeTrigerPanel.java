package rs.mpele.capilaritty;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeListener;

import com.github.sarxos.webcam.Webcam;
import javax.swing.event.ChangeEvent;
import java.awt.Dimension;

public class TimeTrigerPanel
extends JPanel {

	private static final long serialVersionUID = 1L;

	private LocalDateTime mLastExecution;
	protected Timer timer;
	private WickingSetPanel wickingSetPanel;
	private JSpinner spinner;
	private TakePicture tmp;
	private Webcam webcam;

	private File mWorkFolder;
	int counter=0;


	public static void main(String args[]) {

		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				JFrame f= new JFrame("Panel Example");    
				TimeTrigerPanel panel = new TimeTrigerPanel(new WickingSetPanel(new File("Experiment1/"), new GraphPanel()), new File("Experiment1/")); 
				f.getContentPane().add(panel);  
				f.setSize(400,400);    
				f.pack();
				f.setVisible(true);   
				f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


				Measurements measurements = new Measurements(new File("Experiment1/"));
				measurements.load();
			}
		});
	}

	public TimeTrigerPanel(WickingSetPanel pWickingSetPanel, File workFolder) {
		this.wickingSetPanel = pWickingSetPanel;
		this.mWorkFolder = workFolder;
		setMinimumSize(new Dimension(10, 25));
		setMaximumSize(new Dimension(32767, 25));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		JButton btnStartRecording = new JButton("Start recording");
		btnStartRecording.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				File[] listOfFiles = mWorkFolder.listFiles(new FilenameFilter() {
					public boolean accept(File dir, String name) {
						return name.toLowerCase().endsWith(".png");
					}
				});

				if(listOfFiles.length>0){
					int n = JOptionPane.showConfirmDialog(
							null,
							"There is pictures in "+mWorkFolder+". Would you like to continue?",
							"Warning",
							JOptionPane.YES_NO_OPTION);
					if(n == 1){
						return;
					}
					else{
						Arrays.sort(listOfFiles);
						counter= Integer.valueOf(listOfFiles[listOfFiles.length-1].getName().substring(0, listOfFiles[listOfFiles.length-1].getName().indexOf('_'))) +1;
						System.out.println("New counter = " + counter);
					}
				}
				else{
					counter = 0;
				}

				timer = new Timer();
				tmp = new TakePicture();
				timer.schedule(tmp, 0, ((int) spinner.getValue())*1000);

				btnStartRecording.setEnabled(false);
			}
		});
		add(btnStartRecording);

		spinner = new JSpinner();
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if(btnStartRecording.isEnabled()){
					return;
				}
				System.out.println(LocalDateTime.now() + " Resetuje");

				timer.cancel();
				timer = new Timer();
				tmp = new TakePicture();
				//timer.schedule(tmp, 0, ((int) spinner.getValue())*1000);
				timer.scheduleAtFixedRate(tmp, 0, ((int) spinner.getValue())*1000);
			}
		});
		spinner.setModel(new SpinnerNumberModel(new Integer(3), new Integer(1), null, new Integer(2)));
		add(spinner);

		JButton btnStopRecording = new JButton("Stop recording");
		btnStopRecording.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.cancel();
				btnStartRecording.setEnabled(true);
			}
		});
		add(btnStopRecording);


		// Initialization camera
		SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
			@Override
			protected String doInBackground() throws InterruptedException
			{
				for( Webcam cam : Webcam.getWebcams()){
					System.out.println(cam.getName());
					if(cam.getName().contains("USB Video Device") || cam.getName().contains("Webcam")){
						webcam = cam;
					}
				}
				
				if(webcam == null){ //fallback
					webcam = Webcam.getWebcams().get(Webcam.getWebcams().size()-1);						
				}

				//Dimension d = new Dimension(2592, 1944);
				Dimension d = new Dimension(1280, 720);
				webcam.setCustomViewSizes(new Dimension[] { d });
				webcam.setViewSize(d);
				webcam.open();
				return null;

			}
			@Override
			protected void done() {
			}
		};
		worker.execute();		
	}


	class TakePicture extends TimerTask {
		public void run() {
			mLastExecution = LocalDateTime.now();
			System.out.println(LocalDateTime.now()+" Next execution: "+ " " + (int) spinner.getValue() + " " + mLastExecution.getSecond());
			takePicture();
		}
	}

	public void takePicture() {
		BufferedImage image = webcam.getImage();
		image = TmpCamera.rotateCw(image);

		String nameOfFile = mWorkFolder.getName()+"\\"+
				String.format("%04d", counter++) + 
				"_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())+".png";
		try {
			ImageIO.write(image, "PNG", new File(nameOfFile));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			Beep.tone(1000,100);
		} catch (LineUnavailableException e) {
			// no action if there is problem with beep
		}
		wickingSetPanel.addFile(new File(nameOfFile));
	}

	public void setFolder(File workFolder) {
		this.mWorkFolder = workFolder;
	}

	public Webcam getWebcam(){
		return webcam;
	}
}
