package rs.mpele.capilaritty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.TreeMap;

public class Calibration {

	private int y_of_Zero = 1;      // in px
	private int y_of_MaxValue = 0;  // in px
	private double maxValue = 0;       // in mm

	private TreeMap<Integer, ControlPoint> controlPoints = new TreeMap<Integer, ControlPoint>();

	String mFolder = ".";


	public static void main(String[] args) throws ParseException {
		Calibration calibration = new Calibration("./Sneza1NHc/");
		calibration.load();

		int y = 719;
		Double wicking = calibration.getWicking(y);
		System.out.println(y + " " + wicking + " " + calibration.getY(wicking));
		wicking = calibration.getWicking(calibration.getY(wicking));
		
		y = 10;
		wicking = calibration.getWicking(y);
		System.out.println(y + " " + wicking + " " + calibration.getY(wicking));
		
		y = 100;
		wicking = calibration.getWicking(y);
		System.out.println(y + " " + wicking + " " + calibration.getY(wicking));		
		y = 300;
		wicking = calibration.getWicking(y);
		System.out.println(y + " " + wicking + " " + calibration.getY(wicking));		
		y = 500;
		wicking = calibration.getWicking(y);
		System.out.println(y + " " + wicking + " " + calibration.getY(wicking));

	}



	public Calibration(String folder) {
		mFolder = folder;
	}

	public Calibration(int y_of_Zero, int y_of_MaxValue, int maxValue) {
		super();
		this.y_of_Zero = y_of_Zero;
		this.y_of_MaxValue = y_of_MaxValue;
		this.maxValue = maxValue;		
	}

	public Calibration() {
	}

	public Calibration(Calibration calibration) {
		y_of_Zero = calibration.getY_of_Zero();
		y_of_MaxValue = calibration.getY_of_MaxValue();
		maxValue = calibration.getMaxValue();
		mFolder = calibration.getFolder();
		controlPoints = calibration.getControlPoints();    // TODO  !!!!!!!!!!!!!! Proveriti da li je po vrednosti ili po refernci !!!!!!!!!!!!!!!!!!!!!!!!!
	}

	public void setCalibration(Calibration calibration) {
		y_of_Zero = calibration.getY_of_Zero();
		y_of_MaxValue = calibration.getY_of_MaxValue();
		maxValue = calibration.getMaxValue();
		mFolder = calibration.getFolder();
		controlPoints = calibration.getControlPoints();    // TODO  !!!!!!!!!!!!!! Proveriti da li je po vrednosti ili po refernci !!!!!!!!!!!!!!!!!!!!!!!!!
	}

	public void setFolder(File folder2) {
		this.mFolder = folder2.getPath();
	}

	public String getFolder() {
		return mFolder;
	}

	public int getY_of_Zero() {
		return y_of_Zero;
	}
	public void setY_of_Zero(int y_of_Zero) {
		this.y_of_Zero = y_of_Zero;
	}
	public int getY_of_MaxValue() {
		return y_of_MaxValue;
	}
	public void setY_of_MaxValue(int y_of_MaxValue) {
		this.y_of_MaxValue = y_of_MaxValue;
	}
	public double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(int maxValue) {
		this.maxValue = maxValue;
	}


	public TreeMap<Integer, ControlPoint> getControlPoints() {
		return controlPoints;
	}
	public void setControlPoint(int i, ControlPoint point){
		controlPoints.put(i, point);
	}
	public ControlPoint getControlPoint(int i) {
		return controlPoints.get(i);		
	}
	public void removeControlPoint(int i) {
		controlPoints.remove(i);		
	}


	public int getY(double value){
		double upperValue = maxValue;
		double downValue = 0;
		int y_upper = y_of_MaxValue;
		int y_down = y_of_Zero;

		if(!controlPoints.isEmpty() && controlPoints.firstEntry().getValue().isActive()){
			if(value > controlPoints.firstEntry().getValue().getValue()){
				y_down = controlPoints.firstEntry().getValue().getY_position();
				downValue = controlPoints.firstEntry().getValue().getValue();
			}
			else{
				y_upper = controlPoints.firstEntry().getValue().getY_position();
				upperValue = controlPoints.firstEntry().getValue().getValue();
			}
		}

		return  (int) (y_down +(y_upper-y_down)*(value-downValue)/(upperValue-downValue));
	}

	public Double getWicking(int y){
		double upperValue = maxValue;
		double downValue = 0;
		int y_upper = y_of_MaxValue;
		int y_down = y_of_Zero;

		if(!controlPoints.isEmpty() && controlPoints.firstEntry().getValue().isActive()){
			if(y < controlPoints.firstEntry().getValue().getY_position()){
				y_down = controlPoints.firstEntry().getValue().getY_position();
				downValue = controlPoints.firstEntry().getValue().getValue();
			}
			else{
				y_upper = controlPoints.firstEntry().getValue().getY_position();
				upperValue = controlPoints.firstEntry().getValue().getValue();
			}
		}

		double tmp = downValue +  (upperValue - downValue) * (y - y_down) / (y_upper-y_down);

		DecimalFormat df = new DecimalFormat("#.#");      
		tmp = Double.valueOf(df.format(tmp));
		return tmp;
	}

	public void save() throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(mFolder+"/calibration.txt")) {
			out.println("y_of_Zero=" + y_of_Zero + ",y_of_MaxValue=" + y_of_MaxValue + ",maxValue=" + maxValue);
			if(controlPoints.firstEntry() != null){
				out.println("correction;" + controlPoints.firstEntry().getValue().getY_position() + ";" + controlPoints.firstEntry().getValue().getValue());
			}
		}
	}	

	public void load() {
		File file = new File(mFolder+"/calibration.txt");

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));
			String st = br.readLine();

			String[] splited = st.split(",");
			y_of_Zero = Integer.valueOf(splited[0].split("=")[1]);
			y_of_MaxValue = Integer.valueOf(splited[1].split("=")[1]);
			maxValue = Double.valueOf(splited[2].split("=")[1]);


			controlPoints = new TreeMap<Integer, ControlPoint>();
			st = br.readLine();
			if(st != null){
				String[] splited2 = st.split(";");
				controlPoints.put(1, new ControlPoint(true, Integer.valueOf(splited2[1]), Double.valueOf(splited2[2])));
			}


		} catch (IOException e) {
			System.out.println("There is no file with section definition.");
		}

		System.out.println(this.toString());
	}



	@Override
	public String toString() {
		return "Calibration [y_of_Zero=" + y_of_Zero + ", y_of_MaxValue=" + y_of_MaxValue + ", maxValue=" + maxValue + "]";
	}


}
