package rs.mpele.capilaritty.exportToMp4;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;

import javax.imageio.ImageIO;
import org.jcodec.api.awt.AWTSequenceEncoder;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.io.SeekableByteChannel;
import org.jcodec.common.model.Rational;

import rs.mpele.capilaritty.Measurements;

public class ExportToMP4 {

	public static void main(String[] args)  {
		ExportToMP4.createMP4("Jasna2430");
	}
	
	public static void createMP4(String folderName){

		File folder = new File(folderName);
		File[] listOfFiles = folder.listFiles();
		Arrays.sort(listOfFiles);

		Date mReferentTime = Measurements.getTimeFromFilename(listOfFiles[0].getName());

		SeekableByteChannel out = null;
		try {
			out = NIOUtils.writableFileChannel("output.mp4");
			AWTSequenceEncoder encoder = new AWTSequenceEncoder (out, Rational.R(10, 1));
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].getName().contains(".png")) {

					System.out.println(listOfFiles[i].getPath());

					Date currentTime = Measurements.getTimeFromFilename(listOfFiles[i].getName());

					// Generate the image, for Android use Bitmap
					BufferedImage image = ImageIO.read(new File(listOfFiles[i].getPath()));

					Graphics2D g2d = image.createGraphics();
			        g2d.setPaint(Color.red);
			        g2d.setFont(new Font("Serif", Font.BOLD, 50));
			        String s = "Time: " + LocalTime.MIN.plusSeconds(Measurements.secondsOfExperiment(currentTime, mReferentTime)).format(DateTimeFormatter.ISO_LOCAL_TIME);
			        
			        FontMetrics fm = g2d.getFontMetrics();
			        int x = image.getWidth() - fm.stringWidth(s) - 50;
			        int y = fm.getHeight();
			        g2d.drawString(s, x, y);
			        g2d.dispose();
					
					// Encode the image
					encoder.encodeImage(image);
					encoder.encodeImage(image); // To slow down
//					encoder.encodeImage(image);
				}
			}
			// Finalize the encoding, i.e. clear the buffers, write the header, etc.
			encoder.finish();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			NIOUtils.closeQuietly(out);
		}
		
		System.out.println("Done");
	}

}
