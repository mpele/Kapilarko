package rs.mpele.capilaritty;

import javax.swing.JPanel;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import net.miginfocom.swing.MigLayout;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.Component;
import javax.swing.Box;
import java.awt.Insets;

public class WickingSetPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private MarvinImagePanel mImagePanelModified;
	private MarvinImagePanel mImagePanelOriginal;
	private int mCurrentSlide = 0;
	private ArrayList<Long> mTimes;
	private WickingSet wickingSet;
	private JLabel lblTime;
	private JTextField textFieldValue;
	private JLabel lblWicking;
	private JPanel panel;
	private JButton btnSave;
	private JCheckBox chckbxManualy;
	private JButton btnLast;
	private JButton btnFirst;
	private JLabel lblSeconds;
	private GraphPanel mGraphPanel;
	private HistogramPanel histogramPanel;
	private JButton btnDeletePhoto;
	private JScrollPane scrollPane;
	private JLabel lblThresholdMetod;
	private Component horizontalStrut;
	private JButton btnNext;
	private JButton button;
	private JButton button_1;


	public WickingSetPanel(File workFolder, GraphPanel graphPanel) {
		setLayout(new MigLayout("", "[][]", "[][][]"));

		setFolder(workFolder);
		this.mGraphPanel = graphPanel;

		mImagePanelOriginal = new MarvinImagePanel();
		mImagePanelModified = new MarvinImagePanel();

		if(!mTimes.isEmpty() &&  wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)) != null){
			mImagePanelOriginal.setImage(wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).getOriginalPhoto());
			mImagePanelModified.setImage(wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).getOriginalPhoto());
		}

		JPanel tmpForScroll = new JPanel();

		tmpForScroll.add(mImagePanelOriginal);
		tmpForScroll.add(mImagePanelModified);

		scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBorder(null);
		scrollPane.setViewportView(tmpForScroll);
		add(scrollPane, "cell 0 0");


		JButton btnPrevius = new JButton("Previus");
		btnPrevius.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(mCurrentSlide>0 ) {
					mCurrentSlide--;
					updatePanel();
				}
			}
		});

		btnFirst = new JButton("First");
		btnFirst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mCurrentSlide=0;
				updatePanel();
			}
		});

		btnDeletePhoto = new JButton("Delete current photo");
		btnDeletePhoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				File file = new File(wickingSet.getFolder() + "/"+wickingSet.getMeasurements().getFile(mTimes.get(mCurrentSlide)));

				if(file.delete())
				{
					System.out.println("File deleted successfully - "+ workFolder+"/"+wickingSet.getMeasurements().getFile(mTimes.get(mCurrentSlide)));
				}
				else
				{
					System.out.println("Failed to delete the file - "+ workFolder+"/"+wickingSet.getMeasurements().getFile(mTimes.get(mCurrentSlide)));
					JOptionPane.showMessageDialog(null,
							"Failed to delete the file !!!",
							"Inane error",
							JOptionPane.ERROR_MESSAGE);
				}

				try {
					wickingSet.deleteMeasurement(mTimes.get(mCurrentSlide));
				} catch (FileNotFoundException e) {
					JOptionPane.showMessageDialog(null,
							"Failed to save measurement !!!",
							"Inane error",
							JOptionPane.ERROR_MESSAGE);					e.printStackTrace();
				}

				updadeDataFromWickingSet();
				mGraphPanel.setMeasurements(wickingSet.getMeasurements());
				btnNext.requestFocus();
				updatePanel();
			}
		});
		add(btnDeletePhoto, "cell 1 1");
		add(btnFirst, "flowx,cell 0 2");

		button_1 = new JButton("<<");
		button_1.setMargin(new Insets(2, 0, 2, 0));
		button_1.setAlignmentY(0.0f);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mCurrentSlide>10 ) {
					mCurrentSlide = mCurrentSlide - 10;
				}
				else{
					mCurrentSlide = 0;
				}
				updatePanel();
			}
		});
		add(button_1, "cell 0 2");
		add(btnPrevius, "cell 0 2");

		btnNext = new JButton("Next");
		btnNext.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mCurrentSlide + 1 < mTimes.size()) {
					mCurrentSlide++;
					updatePanel();
				}
			}
		});
		add(btnNext, "cell 0 2");

		lblTime = new JLabel("-");
		add(lblTime, "flowy,cell 1 0");

		lblSeconds = new JLabel("-");
		add(lblSeconds, "cell 1 0");

		panel = new JPanel();
		add(panel, "cell 1 0");
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		lblWicking = new JLabel("Wicking: ");
		panel.add(lblWicking);

		textFieldValue = new JTextField();
		textFieldValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSave.doClick();
			}
		});
		textFieldValue.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				chckbxManualy.setSelected(true);
			}
		});
		panel.add(textFieldValue);
		textFieldValue.setColumns(5);

		horizontalStrut = Box.createHorizontalStrut(20);
		panel.add(horizontalStrut);

		lblThresholdMetod = new JLabel("-");
		panel.add(lblThresholdMetod);

		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wickingSet.getMeasurements().setWicking(mTimes.get(mCurrentSlide), Double.valueOf(textFieldValue.getText()), true);
				wickingSet.getMeasurements().setWickingManual(mTimes.get(mCurrentSlide), chckbxManualy.isSelected());
				try {
					wickingSet.getMeasurements().save();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				updatePanel();
				mGraphPanel.setMeasurements(wickingSet.getMeasurements());
			}
		});

		chckbxManualy = new JCheckBox("Manualy");
		add(chckbxManualy, "cell 1 0");
		add(btnSave, "cell 1 0,alignx right");

		btnLast = new JButton("Last");
		btnLast.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				mCurrentSlide = mTimes.size()-1;
				updatePanel();
			}
		});

		button = new JButton(">>");
		button.setMargin(new Insets(2, 0, 2, 0));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(mCurrentSlide + 10 < mTimes.size()) {
					mCurrentSlide = mCurrentSlide + 10;
				}
				else{
					mCurrentSlide = mTimes.size() - 1;
				}
				updatePanel();
			}
		});
		add(button, "cell 0 2");
		add(btnLast, "cell 0 2");

		mCurrentSlide = wickingSet.getPosLastSlide();

		histogramPanel = new HistogramPanel("-", new PhotoSection());
		add(histogramPanel, "cell 1 0,grow");

		updatePanel();
	}


	public void setFolder(File workFolder) {
		try {
			wickingSet = new WickingSet(workFolder); 
		} catch (FileNotFoundException | ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		updadeDataFromWickingSet();
	}


	void updadeDataFromWickingSet() {
		mTimes = new ArrayList<>(wickingSet.getPhotoSectionMap().keySet());
		if(mCurrentSlide == wickingSet.getPosLastSlide()-1){
			mCurrentSlide = wickingSet.getPosLastSlide();
			updatePanel();
		}
	}


	void updatePanel(){
		if(mCurrentSlide > mTimes.size()){  
			mCurrentSlide = 0;
		}

		if(mCurrentSlide == 0 && mTimes.size() == 0 ){
			mImagePanelOriginal.setImage(new MarvinImage());
			mImagePanelModified.setImage(new MarvinImage());
			histogramPanel.setData(new PhotoSection());

			lblTime.setText("Time slice: "+LocalTime.MIN.plusSeconds(0).format(DateTimeFormatter.ISO_LOCAL_TIME));
			lblSeconds.setText("-");
			textFieldValue.setText("-");
			chckbxManualy.setSelected(false);
		}
		else{
			lblTime.setText("Time slice: "+LocalTime.MIN.plusSeconds(mTimes.get(mCurrentSlide)).format(DateTimeFormatter.ISO_LOCAL_TIME));
			lblSeconds.setText(wickingSet.getMeasurements().getFile(mTimes.get(mCurrentSlide)));
			textFieldValue.setText(String.valueOf(wickingSet.getMeasurements().getWicking(mTimes.get(mCurrentSlide))));
			lblThresholdMetod.setText(String.valueOf(wickingSet.getThresholdSettings().getThresholdMethod(mTimes.get(mCurrentSlide)))+" "+
					wickingSet.getThresholdSettings().getPosition(mTimes.get(mCurrentSlide)));
			chckbxManualy.setSelected(wickingSet.getMeasurements().getWickingManual(mTimes.get(mCurrentSlide)));
			mGraphPanel.setCurrentValueMarker(mTimes.get(mCurrentSlide));

			if(wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)) != null){
				mImagePanelOriginal.setImage(wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).getOriginalPhoto());
				MarvinImage tmp = wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).getPreparedPhoto();
				if(wickingSet.getMeasurements().getWickingManual(mTimes.get(mCurrentSlide))){
					wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).drawWickingManualy(tmp,wickingSet.getMeasurements().getWicking(mTimes.get(mCurrentSlide)));
				}

				// TODO !!!!!!!!!!!!!!!!!! IZMESTITI OVAJ KOD ODAVDE
				// for neuroph
//				NeuralNetwork loadedMlPerceptron = NeuralNetwork.createFromFile("myMlPerceptron.nnet");
//				int neurophTreshold = wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).getThreshold(loadedMlPerceptron);
//				double value = wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).readWicking(neurophTreshold);
//				System.out.println("neuroph: " + neurophTreshold + "/" +value);
//				wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)).drawWickingManualy(tmp, value);
//				histogramPanel.setAdditionalMarker(neurophTreshold);

				mImagePanelModified.setImage(tmp);
				histogramPanel.setData(wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide)));
			}
			else{
				mImagePanelOriginal.setImage(new MarvinImage());
				mImagePanelModified.setImage(new MarvinImage());
				histogramPanel.setData(new PhotoSection());
			}
		}
	}

	public WickingSet getWickingSet() {
		return wickingSet;
	}


	public void addFile(File file)  {
		try {
			getWickingSet().addFile(file);
			updadeDataFromWickingSet();
			mGraphPanel.addWickingData(mTimes.get(mTimes.size()-1), wickingSet.getMeasurements().getWicking(mTimes.get(mTimes.size()-1)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public String getCurrentPictureFileName() {
		return wickingSet.getMeasurements().getFolder()+"/"+wickingSet.getMeasurements().getFile(mTimes.get(mCurrentSlide));
	}


	public PhotoSection getCurrentPhotoSection() {
		return wickingSet.getPhotoSectionMap().get(mTimes.get(mCurrentSlide));
	}


	public long getCurrentSlideTimeSeconds() {
		return mTimes.get(mCurrentSlide);
	}


}
