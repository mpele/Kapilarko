package rs.mpele.capilaritty;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSpinner;

import marvin.gui.MarvinImagePanel;

import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeListener;

import javax.swing.event.ChangeEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;

import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import javax.swing.JComboBox;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JRadioButton;

public class HistogramTuningDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private HistogramPanel histogramPanel;
	private ThresholdSettings mThresholdSettings;
	private ThresholdSettings mThresholdSettingsOriginal;
	private PhotoSection mPhotoSection;
	private long mTimeSeconds;
	private JSpinner spinner_min;
	private JSpinner spinnerMaxX;
	private JSpinner spinnerVariable1;
	private JSpinner spinnerVariable2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {

			WholePicture pic = new WholePicture("Proba 1 J/0023_2018.08.02.17.49.08.png");
			Section section = new Section("./Proba 1 J/");

			//WholePicture pic = new WholePicture("sKopijaPreRucnog/Sneza3NVc/0153_2018.08.27.08.30.38.png");
			//Section section = new Section("./sKopijaPreRucnog/Sneza3NVc/");

			section.load();
			PhotoSection photoSection = pic.getSection(section);
			
			//Calibration calibration = new Calibration("./sKopijaPreRucnog/Sneza3NVc/");
			Calibration calibration = new Calibration("./Proba 1 J/");
			calibration.load();

			photoSection.setCalibration(calibration);

			ThresholdSettings threshordSetting = new ThresholdSettings(170, 250, 0.95, 100.);

			threshordSetting.setFolder(calibration.getFolder());
			threshordSetting.load();
			photoSection.setThresholdSettings(threshordSetting);
			photoSection.setReferentTime(new Date());

			photoSection.getWicking();



			HistogramTuningDialog dialog = new HistogramTuningDialog(photoSection,500);
			ThresholdSettings res = dialog.showDialog();

			System.out.println("Dialog "+res);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @param photoSection 
	 * @param timeSeconds 
	 */
	public HistogramTuningDialog(PhotoSection photoSection, long timeSeconds) {
		setTitle("Defining threshold method");
		
		mPhotoSection = photoSection;
		mTimeSeconds = timeSeconds;
		mThresholdSettings = new ThresholdSettings(photoSection.getThresholdSettings()); 
		mThresholdSettingsOriginal = new ThresholdSettings(photoSection.getThresholdSettings()); 

		mThresholdSettings.loadParameters(timeSeconds);
		
		// TODO Da li treba da se definise folder ovde
		mThresholdSettings.setFolder(photoSection.getFolder());

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModal(true);

		setBounds(10, 10, 1000, 750);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		MarvinImagePanel imagePanel = new MarvinImagePanel();
		MarvinImagePanel imagePanel2 = new MarvinImagePanel();
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
		contentPanel.add(imagePanel);
		contentPanel.add(imagePanel2);
		imagePanel.setImage(photoSection.getOriginalPhoto());
		imagePanel2.setImage(photoSection.getPreparedPhoto());

		JPanel panel_4 = new JPanel();
		contentPanel.add(panel_4);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));

		//JComboBox<ThresholdMethodEnum> comboBox = new JComboBox<ThresholdMethodEnum>();
		JComboBox<ThresholdMethodEnum> comboBox = new JComboBox<ThresholdMethodEnum>(ThresholdMethodEnum.values());
		comboBox.setSelectedItem(mThresholdSettings.getThresholdMethod(mTimeSeconds));
		comboBox.setMaximumSize(new Dimension(32767, 20));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {			
				photoSection.setThresholdMethod((ThresholdMethodEnum) comboBox.getSelectedItem());
				updatePanel(photoSection, imagePanel2);
			}
		});

		JPanel panel_3 = new JPanel();
		panel_4.add(comboBox);

		JPanel panel_6 = new JPanel();
		panel_4.add(panel_6);
		panel_6.setPreferredSize(new Dimension(10, 40));

		JRadioButton rdbtnWholeRange = new JRadioButton("Whole range");
		panel_6.add(rdbtnWholeRange);

		JRadioButton rdbtnFromThisImage = new JRadioButton("From this image");
		rdbtnFromThisImage.setSelected(true);
		panel_6.add(rdbtnFromThisImage);

		ButtonGroup group = new ButtonGroup();
		group.add(rdbtnWholeRange);
		group.add(rdbtnFromThisImage);



		panel_4.add(panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		JPanel panel_2 = new JPanel();
		panel_2.setMaximumSize(new Dimension(32767, 300));
		panel_3.add(panel_2);

		histogramPanel = new HistogramPanel("XY Series Demo 3",photoSection);
		panel_2.add(histogramPanel);

		JPanel panel_1 = new JPanel();
		panel_2.add(panel_1);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		JPanel panel = new JPanel();
		panel.setMaximumSize(new Dimension(32767, 100));
		panel.setSize(new Dimension(20, 20));
		panel_3.add(panel);

		JPanel panel_7 = new JPanel();
		panel.add(panel_7);

		JLabel labelMin = new JLabel("min x");
		panel_7.add(labelMin);

		 spinner_min = new JSpinner();
		spinner_min.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				photoSection.setMinX((Integer) spinner_min.getValue());
				updatePanel(photoSection, imagePanel2);
			}
		});
		spinner_min.setModel(new SpinnerNumberModel(new Integer(mThresholdSettings.getMin_x()), null, null, new Integer(1)));
		spinner_min.setPreferredSize(new Dimension(50, 20));
		panel_7.add(spinner_min);

		JPanel panel_8 = new JPanel();
		panel.add(panel_8);

		JLabel lblMaxX = new JLabel("max x");
		panel_8.add(lblMaxX);

		 spinnerMaxX = new JSpinner();
		spinnerMaxX.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				photoSection.setMaxX((Integer) spinnerMaxX.getValue());
				updatePanel(photoSection, imagePanel2);
			}
		});
		spinnerMaxX.setModel(new SpinnerNumberModel(new Integer(mThresholdSettings.getMax_x()), null, null, new Integer(1)));
		spinnerMaxX.setPreferredSize(new Dimension(50, 20));
		panel_8.add(spinnerMaxX);

		JPanel panel_Variable1 = new JPanel();
		panel.add(panel_Variable1);

		JLabel lblVariable_1 = new JLabel("Variable1");
		lblVariable_1.setToolTipText("minimal high of bottom of valey on the left side from the peak (% of max)");
		panel_Variable1.add(lblVariable_1);
		 spinnerVariable1 = new JSpinner();
		spinnerVariable1.setPreferredSize(new Dimension(50, 20));
		panel_Variable1.add(spinnerVariable1);
		spinnerVariable1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				photoSection.setVariable1((Double) spinnerVariable1.getValue());
				updatePanel(photoSection, imagePanel2);
			}
		});
		spinnerVariable1.setModel(new SpinnerNumberModel(new Double(mThresholdSettings.getVariable1()), null, null, new Double(0.005)));

		JPanel panel_Variable2 = new JPanel();
		panel.add(panel_Variable2);
		panel_Variable2.setMinimumSize(new Dimension(500, 10));

		JLabel lblVariable = new JLabel("Variable2");
		lblVariable.setToolTipText("min hight on left side of the hole");
		panel_Variable2.add(lblVariable);
		 spinnerVariable2 = new JSpinner();
		spinnerVariable2.setPreferredSize(new Dimension(50, 20));
		panel_Variable2.add(spinnerVariable2);
		spinnerVariable2.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				photoSection.setVariable2((Double) spinnerVariable2.getValue());
				updatePanel(photoSection, imagePanel2);
			}
		});

		spinnerVariable2.setModel(new SpinnerNumberModel(new Double(mThresholdSettings.getVariable2()), null, null, new Double(0.001)));


		JPanel panel_5 = new JPanel();
		panel_5.setMaximumSize(new Dimension(32767, 20));
		panel_3.add(panel_5);

		JSpinner spinnerThresholdGoal = new JSpinner();
		spinnerThresholdGoal.setModel(new SpinnerNumberModel(201, 0, 255, 1));
		panel_5.add(spinnerThresholdGoal);

		JButton btnRecommend = new JButton("Recommend");
		btnRecommend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				double[] histogram = mPhotoSection.getHistogram();
				double maxValue = 0;
				for(int i = 0; i <=255; i++){
					if(maxValue < histogram[i]){
						maxValue = histogram[i];
					}
				}
				int goal = (int) spinnerThresholdGoal.getValue();
				
				updateThresholdSettings();
				
				for(double v1 = 0; v1 <= 1; v1 = v1 + 0.001){
					for(double v2 = 0; v2 <= maxValue; v2 = v2 + 0.010){
						int threslold = PhotoSection.determineThresholdCustom(mPhotoSection.getHistogram(), mThresholdSettings);
						if(threslold > goal - 2 && threslold < goal +2)
							System.out.println("v1="+v1+" "+" v2="+v2+" "+PhotoSection.determineThresholdCustom(mPhotoSection.getHistogram(), new ThresholdSettings(170, 250, v1, 10.)));
					}
				}
				System.out.println("Done.");
			}
		});
		panel_5.add(btnRecommend);



		Component verticalGlue = Box.createVerticalGlue();
		panel_4.add(verticalGlue);

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							
							// reset mTresholdSettings value
							mThresholdSettings = mThresholdSettingsOriginal;
							updateThresholdSettings();
							if(rdbtnWholeRange.isSelected()){
								mThresholdSettings.setThresholdMethod((ThresholdMethodEnum) comboBox.getSelectedItem());
							}
							else{								
								mThresholdSettings.setThresholdMethod(mTimeSeconds, (ThresholdMethodEnum) comboBox.getSelectedItem());
							}

							mThresholdSettings.save();
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
							JOptionPane.showMessageDialog(null,
									"Problem with saving Threshold settings.",
									"Inane error",
									JOptionPane.ERROR_MESSAGE);
						}
						setVisible(false);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mThresholdSettings = null;
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}


	private void updatePanel(PhotoSection photoSection, MarvinImagePanel imagePanel2) {
		imagePanel2.setImage(photoSection.getPreparedPhoto());
		histogramPanel.setData(photoSection);
	}

	public ThresholdSettings showDialog() {
		setVisible(true);
		return mThresholdSettings ;
	}
	
	private void updateThresholdSettings() {
		mThresholdSettings.setMin_x((int) spinner_min.getValue());
		mThresholdSettings.setMax_x((int) spinnerMaxX.getValue());
		mThresholdSettings.setVariable1((Double) spinnerVariable1.getValue());
		mThresholdSettings.setVariable2((Double) spinnerVariable2.getValue());		
	}
}
