package rs.mpele.capilaritty;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;

import com.github.sarxos.webcam.Webcam;

public class TmpCamera {
	static int i = 0;

	public static void main(String[] args) throws IOException {

		Webcam webcam = Webcam.getWebcams().get(Webcam.getWebcams().size()-1);

		//Dimension d = new Dimension(2592, 1944);
		Dimension d = new Dimension(1280, 720);
		webcam.setCustomViewSizes(new Dimension[] { d });
		webcam.setViewSize(d);

		webcam.open();
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				BufferedImage image = webcam.getImage();
				image = rotateCw(image);

				try {
					ImageIO.write(image, "PNG", new File(
							String.format("%04d", i++) + 
							"_" + new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())+".png"));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					Beep.tone(1000,100);
				} catch (LineUnavailableException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}, 0, 3000);
	}
	
	public static BufferedImage rotateCw( BufferedImage img )
	{
	    int         width  = img.getWidth();
	    int         height = img.getHeight();
	    BufferedImage   newImage = new BufferedImage( height, width, BufferedImage.TYPE_3BYTE_BGR );
	 
	    for( int i=0 ; i < width ; i++ )
	        for( int j=0 ; j < height ; j++ )
	            newImage.setRGB( height-1-j, i, img.getRGB(i,j) );
	 
	    return newImage;
	}
}