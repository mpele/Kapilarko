package rs.mpele.capilaritty;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class GraphPanel extends JPanel {

	private static final long serialVersionUID = 8298348634489680374L;

	private XYDataset xyDataset;
	private XYSeries wickingData;

	private XYDataset massDataset;
	private XYSeries massData;

	private JFreeChart chart;

	private Date mReferentTime;

	private ValueMarker marker;

	public static void main(String args[]) {

		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				JFrame f = new JFrame("Panel Example");
				GraphPanel panel = new GraphPanel();
				f.add(panel);
				f.setSize(400, 400);
				f.pack();
				f.setVisible(true);

				Measurements measurements = new Measurements(new File("Sneza2PVb/"));
				measurements.load();
				panel.setMeasurements(measurements);
				// Measurements measurement2 = new Measurements(new
				// File("Test4/"));
				// measurement2.load();
				// panel.setAdditionalMeasurement(measurement2);

				panel.loadMass(new File("Sneza2PVb"));

				panel.addWickingData((long) 25, 99.);

				// panel.clearMassData();

				panel.removePointsOutOfInterest();

				panel.setCurrentValueMarker((long) 234);
			}
		});
	}

	public GraphPanel() {

		wickingData = new XYSeries("Wicking, mm");
		xyDataset = new XYSeriesCollection(wickingData);
		chart = ChartFactory.createXYLineChart(null, "Time, m", "Wicking, m", xyDataset, PlotOrientation.VERTICAL, true,
				true, false);
		ChartPanel cp = new ChartPanel(chart);
		cp.setMouseWheelEnabled(true);
		add(cp);

		// for mass
		final XYPlot plot = chart.getXYPlot();
		massData = new XYSeries("Mass, g");
		massDataset = new XYSeriesCollection(massData);

		plot.setDataset(1, massDataset);
		plot.mapDatasetToRangeAxis(1, 1);

		XYItemRenderer renderer2 = new StandardXYItemRenderer();
		renderer2.setSeriesPaint(0, Color.green);
		plot.setRenderer(1, renderer2);

		final ValueAxis axis2 = new NumberAxis("Mass, g");
		plot.setRangeAxis(1, axis2);
		plot.setRangeAxisLocation(1, AxisLocation.BOTTOM_OR_RIGHT);

		mReferentTime = new Date();

	}

	public void addWickingData(Long time, Double wicking) {
		wickingData.add(time / 60., wicking);
	}

	public void addMassData(double mass) {
		massData.add(Measurements.secondsOfExperiment(new Date(), mReferentTime) / 60., mass);
	}

	protected void setMeasurements(Measurements measurements) {
		Set<Long> times = measurements.getTimes();
		wickingData.clear();

		for (Long time : times) {
			wickingData.add(time / 60., measurements.getWicking(time));
		}

		mReferentTime = measurements.getReferentTime();
	}

	protected void setAdditionalMeasurement(Measurements measurement) {
		Set<Long> times = measurement.getTimes();
		XYSeries newWickingData = new XYSeries(measurement.getFolder());
		newWickingData.clear();

		for (Long time : times) {
			newWickingData.add(time / 60., measurement.getWicking(time));
		}
		((XYSeriesCollection) xyDataset).addSeries(newWickingData);
	}

	protected void loadMass(File folder) {
		clearMassData();
		File file = new File(folder + "/mass.txt");
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String sr;
			while ((sr = br.readLine()) != null) {

				String[] splited = sr.split(";");
				Long time = Measurements.secondsOfExperiment(
						new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(splited[0]), mReferentTime);
				massData.add(time / 60., Double.valueOf(splited[1]));
			}

		} catch (IOException | ParseException e) {
			System.out.println("loadMass: There is no file with measurements definition.");
		}

	}

	protected void removePointsOutOfInterest() {
		int total = massData.getItemCount();
		double lastValue = massData.getDataItem(total - 1).getYValue();

		for (int i = total - 1; i >= 0; i--) {
			if (massData.getDataItem(i).getYValue() < lastValue - 1.)
				massData.remove(i);
		}


		final XYPlot plot = chart.getXYPlot();
		plot.getRangeAxis(1).setRange(massData.getMinY(), massData.getMaxY());
	}

	protected void clearMassData() {
		massData.clear();
	}

	public void setCurrentValueMarker(Long time) {
		if(marker == null){
			marker = new ValueMarker(time / 60.);  // position is the value on the axis
			marker.setPaint(Color.BLUE);
			//marker.setLabel("here"); // see JavaDoc for labels, colors, strokes
		}
		else{
			marker.setValue(time / 60.);
		}

		XYPlot plot = (XYPlot) chart.getPlot();
		plot.addDomainMarker(marker);		
	}
}
