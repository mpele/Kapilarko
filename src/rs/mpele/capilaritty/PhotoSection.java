package rs.mpele.capilaritty;

import java.awt.Color;
import java.util.Arrays;
import java.util.Date;

import javax.swing.JFrame;

import marvin.gui.MarvinImagePanel;
import marvin.image.MarvinImage;
import rs.mpele.capilaritty.neuroph.Utils;

import static marvin.MarvinPluginCollection.*;

import java.awt.FlowLayout;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JPanel;

import org.neuroph.core.NeuralNetwork;

import ij.IJ;
import ij.ImagePlus;

public class PhotoSection {

	private static HistogramPanel histogramPanel;
	private MarvinImage mOriginalPhoto;
	private MarvinImage mPreparedPhoto;

	private Date mTime;
	private Date mReferentTime;

	private Double mWicking = (double) -999; // in mm
	private boolean mManualWicking = false;

	private Calibration mCalibration;
	private int mThreshold = 200;
	private double[] histogram = new double[256];
	private ThresholdSettings thresholdSettings;

	public static void main(String[] args) throws ParseException {
		WholePicture pic = new WholePicture("H:\\Jovanina merenja\\SredjenoDragana\\BAMCoBa\\0121_2018.11.23.14.31.37.png");

		Section section = new Section("H:\\Jovanina merenja\\SredjenoDragana\\BAMCoBa\\");
		section.load();
		PhotoSection photoSection = pic.getSection(section);

		Calibration calibration = new Calibration("H:\\Jovanina merenja\\SredjenoDragana\\BAMCoBa\\");
		calibration.load();

		photoSection.setCalibration(calibration);

		ThresholdSettings threshordSetting = new ThresholdSettings(170, 250, 0.05, 0.01);
		threshordSetting.setFolder(calibration.getFolder());
		threshordSetting.load();
		photoSection.setThresholdSettings(threshordSetting);
		photoSection.setReferentTime(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse("2018.06.23.13.51.04"));


		long startTime = System.currentTimeMillis();

		double tmpWickig = photoSection.getWicking();

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println("readWicking time:" + elapsedTime);

		System.out.println("Time, s: " + photoSection.getSecondOfExperiment());
		System.out.println("Wicking, mm: " + tmpWickig);
		JFrame frame = new JFrame("JFrame");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel);

		MarvinImagePanel imagePanel = new MarvinImagePanel();
		MarvinImagePanel imagePanel2 = new MarvinImagePanel();
		panel.add(imagePanel);
		panel.add(imagePanel2);
		imagePanel.setImage(photoSection.getOriginalPhoto());
		MarvinImage tmp = photoSection.getPreparedPhoto();
		//photoSection.drawWickingManualy(tmp, 150.);
		
		NeuralNetwork<?> loadedMlPerceptron = NeuralNetwork.createFromFile("myMlPerceptron.nnet");
		int neurophTreshold = photoSection.getThreshold(loadedMlPerceptron);
		double value = photoSection.readWicking(neurophTreshold);
		
		System.out.println("vrednosti neuroph: " +neurophTreshold + "/" +value);
		photoSection.drawWickingManualy(tmp, value);
		imagePanel2.setImage(tmp);

		histogramPanel = new HistogramPanel("-", photoSection);
		panel.add(histogramPanel);

		frame.setBounds(10, 10, 700, 900);
		frame.setVisible(true);
	}

	public int getThreshold(NeuralNetwork<?> nnet) {
		int from = 255 - nnet.getInputsCount();
		
		double[] newArray = Arrays.copyOfRange(getHistogram(), from, 255);
		nnet.setInput(newArray);
		nnet.calculate();
		double[ ] networkOutput = nnet.getOutput();
		return Utils.getThreshold(150, 255, networkOutput[0]);
	}

	public void setCalibration(Calibration calibration) {
		mCalibration = calibration;
	}

	public void setOriginalPhoto(MarvinImage mOriginalPhoto) {
		this.mOriginalPhoto = mOriginalPhoto;
	}

	public MarvinImage getOriginalPhoto() {
		MarvinImage tmp = mOriginalPhoto.clone();
		//scale(tmp.clone(), tmp, 100);
		return tmp;
	}

	public void startPrepearingPhoto() {
		mPreparedPhoto = mOriginalPhoto.clone();
	}

	public MarvinImage getPreparedPhoto() {
		// startPrepearingPhoto();
		// colorChannel(mPreparedPhoto, mPreparedPhoto, 0, 50, 50);
		// mPreparedPhoto.resize(mPreparedPhoto.getWidth(),
		// mPreparedPhoto.getHeight());
		// grayScale(mPreparedPhoto);
		// thresholding(mPreparedPhoto, mThreshold);

		MarvinImage tmp = mPreparedPhoto.clone();
		drawRuler(tmp);
		drawWicking(tmp);
		//scale(tmp.clone(), tmp, 100);
		return tmp;
	}

	public void drawRuler(MarvinImage tmp) {
		try {
			tmp.drawLine(0, mCalibration.getY_of_Zero(), tmp.getWidth() - 1, mCalibration.getY_of_Zero(), Color.RED);
			tmp.drawLine(0, mCalibration.getY_of_MaxValue(), tmp.getWidth() - 1, mCalibration.getY_of_MaxValue(),
					Color.RED);
			for (int i = 0; i < mCalibration.getMaxValue(); i = i + 10) {
				tmp.drawLine(40, mCalibration.getY(i), tmp.getWidth() - 40, mCalibration.getY(i), Color.RED);
			}
		} catch (Exception e) {
			System.out.println("Problem with drawRuler()...");
		}
	}

	private void drawWicking(MarvinImage tmp) {
		try {
			tmp.drawLine(0, mCalibration.getY(mWicking), tmp.getWidth() - 1, mCalibration.getY(mWicking), Color.GREEN);
		} catch (Exception e) {
			System.out.println("Problem with drawWicking...");
		}
	}

	public void drawWickingManualy(MarvinImage tmp, Double manualWicking) {
		try {
			tmp.drawLine(0, mCalibration.getY(manualWicking), tmp.getWidth() - 1, mCalibration.getY(manualWicking), Color.BLUE);
		} catch (Exception e) {
			System.out.println("Problem with drawWicking...");
		}
	}

	public void setTime(Date time) {
		mTime = time;
	}

	public long getSecondOfExperiment() {
		return Measurements.secondsOfExperiment(mTime, mReferentTime);
	}

	public void setWickingManual(Double wicking) {
		this.mWicking = wicking;
		this.mManualWicking = true;
	}

	public void setWicking(Double wicking) {
		this.mWicking = wicking;
	}

	public boolean isManualWicking() {
		return mManualWicking;
	}

	public void setManualWicking(boolean manualWicking) {
		this.mManualWicking = manualWicking;
	}

	/**
	 * Return previously calculated value if it exist. If not - call readWicking
	 * 
	 * @return
	 */
	public Double getWicking() {
		if (mWicking == -999) {
			return readWicking();
		} else {
			return mWicking;
		}
	}

	/**
	 * Recalculate wicking
	 * 
	 * @return
	 */
	public Double readWicking(int threshold) {
		return readWicking(threshold, false);
	}
	
	/**
	 * Recalculate wicking
	 * 
	 * @return
	 */
	public Double readWicking(int threshold, boolean silent) {
		startPrepearingPhoto();
		grayScale(mPreparedPhoto);

		createHistogram(mPreparedPhoto, true);

		if(!silent){
			System.out.println("Using threshlod: " + threshold + " (" + thresholdSettings.getThresholdMethod(this.getSecondOfExperiment()) + ")");
		}
		
		thresholdPhoto(mPreparedPhoto, threshold);

		return readWickingFromThresholdedPhoto();
	}
	
	/**
	 * Recalculate wicking
	 * 
	 * @return
	 */
	public Double readWicking() {
		startPrepearingPhoto();
		// colorChannel(mPreparedPhoto, mPreparedPhoto, 0, 50, 50);
		grayScale(mPreparedPhoto);

		createHistogram(mPreparedPhoto, true);
		mThreshold = determineThreshold(histogram, thresholdSettings);

		System.out.println("calculated threshlod: " + mThreshold + " "
				+thresholdSettings.getThresholdMethod(this.getSecondOfExperiment()));
		// thresholding(mPreparedPhoto, mThreshold);
		thresholdPhoto(mPreparedPhoto, mThreshold);

		return readWickingFromThresholdedPhoto();
	}

	private Double readWickingFromThresholdedPhoto() {
		for (int y = mPreparedPhoto.getHeight() - 1; y > 0; y--) {
			int counter = 0;
			for (int x = 0; x < mPreparedPhoto.getWidth() - 1; x++) {
				if (mPreparedPhoto.getIntComponent0(x, y) < 100) {
					counter++;
				}
			}
			if (counter < mPreparedPhoto.getWidth() * .5) {
				mWicking = mCalibration.getWicking(y);
				// System.out.println(y + " prazno "+mWicking);
				break;
			}
		}
		return mWicking;
	}

	public int determineThreshold(double[] pHistogram, ThresholdSettings pThresholdSettings) {
		if(pThresholdSettings.getThresholdMethod(getSecondOfExperiment())==ThresholdMethodEnum.CUSTOM){
			return determineThresholdCustom(pHistogram, pThresholdSettings);
		}
		else{
			ImagePlus image = new ImagePlus("naslov", getOriginalPhoto().getBufferedImage());
			IJ.run(image, "8-bit", "");

			//IJ.run(image, "HSB Stack", "");
			//image.show();
			//image.setSlice(3);

			IJ.setAutoThreshold(image, pThresholdSettings.getThresholdMethod(getSecondOfExperiment()).toString()+" dark");

			return (int) image.getProcessor().getMinThreshold();
		}
	}


	public static int determineThresholdCustom(double[] pHistogram, ThresholdSettings pThresholdSettings) {
		double min = 99999;
		int posMin = 255;
		int posMax = 0;
		double max = 1;
		boolean maxPeak = false;

		int x = pThresholdSettings.getMax_x();

		for (; x > pThresholdSettings.getMin_x(); x--) {
			if (!maxPeak) {
				if ((max < pHistogram[x]) || max < 150) {
					max = pHistogram[x];
					posMax = x;
					posMin = posMax;
				} else {
					if (max - pHistogram[x] > max * pThresholdSettings.getVariable1()) { //variable1 - minimal depth on the left side from peak (% of max)
						min = pHistogram[x];
						maxPeak = true;
					}
				}
			} else {
				if(min > pHistogram[x]){
					min = pHistogram[x];
					posMin = x;
				}
				else if ( pHistogram[x] - min > max * pThresholdSettings.getVariable2()  && (posMin - x > 4) ) { // variable2 - min hight on left side of hole
					break;
				}
			}
		}

		return posMin;
	}

	private void createHistogram(MarvinImage photo, boolean normalized) {
		histogram = new double[256];
		int totalPixels = photo.getWidth() * photo.getHeight();
		for (int x = 0; x < photo.getWidth(); x++) {
			for (int y = 0; y < photo.getHeight(); y++) {
				if(normalized){
					histogram[photo.getIntComponent0(x, y)] += 1./totalPixels;
				}
				else{
					histogram[photo.getIntComponent0(x, y)]++;
				}
			}
		}
	}

	static public void thresholdPhoto(MarvinImage pPreparedPhoto, int pThreshold) {
		for (int x = 0; x < pPreparedPhoto.getWidth(); x++) {
			for (int y = 0; y < pPreparedPhoto.getHeight(); y++) {
				if (pPreparedPhoto.getIntComponent0(x, y) > pThreshold) {
					pPreparedPhoto.setIntColor(x, y, 255, 255, 255);
				} else {
					pPreparedPhoto.setIntColor(x, y, 0, 0, 0);
				}

			}
		}
	}

	public double[] getHistogram() {
		return histogram;
	}

	public int getThreshold() {
		return mThreshold;
	}

	public void setMinX(Integer value) {
		thresholdSettings.setMin_x(value);
		readWicking();		
	}

	public void setMaxX(Integer value) {
		thresholdSettings.setMax_x(value);
		readWicking();		
	}
	
	public void setVariable2(Double valueOf) {
		thresholdSettings.setVariable2(valueOf);
		readWicking();
	}

	public void setVariable1(Double value) {
		thresholdSettings.setVariable1(value);
		readWicking();		
	}

	public void setThresholdSettings(ThresholdSettings threshordSettings) {
		this.thresholdSettings = threshordSettings;
	}

	public ThresholdSettings getThresholdSettings() {
		return thresholdSettings;
	}

	public String getFolder() {
		return mCalibration.getFolder();
	}

	public void setThresholdMethod(ThresholdMethodEnum selectedItem) {
		thresholdSettings.setThresholdMethod(selectedItem);
		readWicking();
	}
	
	public ThresholdMethodEnum getThresholdMethod(long second) {
		thresholdSettings.loadThresholdParameters(second);
		return thresholdSettings.getThresholdMethod(second);
	}

	public void setReferentTime(Date referentTime) {
		this.mReferentTime = referentTime;
	}
}