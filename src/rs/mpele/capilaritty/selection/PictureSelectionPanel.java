
package rs.mpele.capilaritty.selection;

import java.awt.*;
import javax.swing.*;

import rs.mpele.capilaritty.Calibration;
import rs.mpele.capilaritty.ControlPoint;
import rs.mpele.capilaritty.Section;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.border.TitledBorder;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;


public class PictureSelectionPanel extends JPanel implements ChangeListener2 {

	private static final long serialVersionUID = 1L;
	private PictureSelectionChoser pictureChoser;
	private JTextField textFieldValue;
	private Section mSection;
	private Calibration mCalibration;
	protected JSpinner spinnerUpperPosition;
	private JSpinner spinnerButtomEdge;
	private JSpinner spinnerWidth;
	private JSpinner spinnerLeftEdge;
	private JTextField textField_additionalPoint;


	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				JFrame frame = new JFrame("Test");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				Section section = new Section();
				section.load();
				Calibration calibration = new Calibration();
				calibration.load();

				JComponent newContentPane = new PictureSelectionPanel("./Proba2/0177_2018.08.02.18.43.53.png", section, calibration);
				newContentPane.setOpaque(true); 
				frame.setContentPane(newContentPane);

				//Display the window.
				frame.pack();
				frame.setVisible(true);
			}
		});
	}


	public PictureSelectionPanel(String pFile, Section section2, Calibration calibration) {
		setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

		ImageIcon image = new ImageIcon(pFile);

		mSection = new Section(section2);
		mCalibration = new Calibration(calibration);		

		pictureChoser = new PictureSelectionChoser(mSection, mCalibration, image, 10);
		pictureChoser.setChangeListener(this);
		JScrollPane pictureScrollPane = new JScrollPane(pictureChoser);
		pictureScrollPane.setPreferredSize(new Dimension(800, 500));
		pictureScrollPane.setViewportBorder(BorderFactory.createLineBorder(Color.black));

		add(pictureScrollPane);
		setBorder(BorderFactory.createEmptyBorder(20,20,20,20));

		JPanel panel = new JPanel();
		panel.setMaximumSize(new Dimension(200, 32767));
		add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Section", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.add(panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));

		JLabel lblUpperPosition_1 = new JLabel("Upper position");
		panel_3.add(lblUpperPosition_1);

		spinnerUpperPosition = new JSpinner();
		spinnerUpperPosition.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				mSection.setStartY((int) spinnerUpperPosition.getValue());
				pictureChoser.updateUI();
			}
		});
		spinnerUpperPosition.setMaximumSize(new Dimension(32767, 20));
		spinnerUpperPosition.setModel(new SpinnerNumberModel(mSection.getStartY(), null, null, new Integer(1)));
		panel_3.add(spinnerUpperPosition);

		JLabel lblHight = new JLabel("Hight");
		panel_3.add(lblHight);

		spinnerButtomEdge = new JSpinner();
		spinnerButtomEdge.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				mSection.setHight((int) spinnerButtomEdge.getValue());
				pictureChoser.updateUI();
			}
		});
		spinnerButtomEdge.setMaximumSize(new Dimension(32767, 20));
		spinnerButtomEdge.setModel(new SpinnerNumberModel(mSection.getHight(), null, null, new Integer(1)));
		panel_3.add(spinnerButtomEdge);

		JLabel lblLeftEdge = new JLabel("Left edge");
		panel_3.add(lblLeftEdge);

		spinnerLeftEdge = new JSpinner();
		spinnerLeftEdge.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				mSection.setStartX((int) spinnerLeftEdge.getValue());
				pictureChoser.updateUI();
			}
		});
		spinnerLeftEdge.setMaximumSize(new Dimension(32767, 20));
		spinnerLeftEdge.setModel(new SpinnerNumberModel(mSection.getStartX(), null, null, new Integer(1)));
		panel_3.add(spinnerLeftEdge);

		JLabel lblRightEdge = new JLabel("Width");
		panel_3.add(lblRightEdge);

		spinnerWidth = new JSpinner();
		spinnerWidth.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				mSection.setWidth((int) spinnerWidth.getValue());
				pictureChoser.updateUI();
			}
		});
		spinnerWidth.setMaximumSize(new Dimension(32767, 20));
		spinnerWidth.setModel(new SpinnerNumberModel(mSection.getWidth(), null, null, new Integer(1)));

		panel_3.add(spinnerWidth);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Calibration", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

		JLabel lblBottomPosition = new JLabel("Top position");
		panel_2.add(lblBottomPosition);

		JSpinner spinner = new JSpinner();
		panel_2.add(spinner);
		spinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				mCalibration.setY_of_MaxValue((int) spinner.getValue());
				pictureChoser.updateUI();
			}
		});
		spinner.setMaximumSize(new Dimension(32767, 20));
		spinner.setModel(new SpinnerNumberModel(mCalibration.getY_of_MaxValue(), null, null, new Integer(1)));

		JLabel lblBottonPosition = new JLabel("Bottom position");
		panel_2.add(lblBottonPosition);


		JSpinner spinner_1 = new JSpinner();
		panel_2.add(spinner_1);
		spinner_1.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				mCalibration.setY_of_Zero((int) spinner_1.getValue());
				pictureChoser.updateUI();
			}
		});
		spinner_1.setMaximumSize(new Dimension(32767, 20));
		spinner_1.setModel(new SpinnerNumberModel(mCalibration.getY_of_Zero(), new Integer(0), null, new Integer(1)));

		JLabel lblValue = new JLabel("Value, mm");
		panel_2.add(lblValue);

		textFieldValue = new JTextField();
		textFieldValue.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				mCalibration.setMaxValue(Integer.valueOf(textFieldValue.getText()));
				pictureChoser.updateUI();
			}
		});
		panel_2.add(textFieldValue);
		textFieldValue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mCalibration.setMaxValue(Integer.valueOf(textFieldValue.getText()));
				pictureChoser.updateUI();
			}
		});
		textFieldValue.setMaximumSize(new Dimension(2147483647, 20));
		textFieldValue.setText(String.valueOf(mCalibration.getMaxValue()));

		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);

		Box verticalBox = Box.createVerticalBox();
		panel_4.add(verticalBox);

		JLabel lblCorrection = new JLabel("Correction");
		lblCorrection.setAlignmentX(Component.CENTER_ALIGNMENT);
		verticalBox.add(lblCorrection);

		Box horizontalBox_1 = Box.createHorizontalBox();
		verticalBox.add(horizontalBox_1);


		JCheckBox chckbxEnabled_additionalPoint = new JCheckBox("Enabled");
		if(mCalibration.getControlPoint(1)!=null){
			chckbxEnabled_additionalPoint.setSelected(mCalibration.getControlPoint(1).isActive());
		}

		JSpinner spinner_AdditionalPoint = new JSpinner();
		spinner_AdditionalPoint.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(chckbxEnabled_additionalPoint.isSelected()){
					calibration.getControlPoint(1).setY_position((int) spinner_AdditionalPoint.getValue());
					pictureChoser.updateUI();
				}
			}
		});


		chckbxEnabled_additionalPoint.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(chckbxEnabled_additionalPoint.isSelected()){
					calibration.setControlPoint(1, new ControlPoint(true, 
							(int) spinner_AdditionalPoint.getValue(), 
							Double.valueOf(textField_additionalPoint.getText())));
				}
				else{
					calibration.removeControlPoint(1);
				}

				pictureChoser.updateUI();
			}
		});
		horizontalBox_1.add(chckbxEnabled_additionalPoint);

		Box horizontalBox = Box.createHorizontalBox();
		verticalBox.add(horizontalBox);

		textField_additionalPoint = new JTextField();
		horizontalBox.add(textField_additionalPoint);
		textField_additionalPoint.setPreferredSize(new Dimension(50, 20));
		textField_additionalPoint.setMinimumSize(new Dimension(100, 20));
		if(mCalibration.getControlPoint(1)!=null){
			textField_additionalPoint.setText(mCalibration.getControlPoint(1).getValue().toString());
		}
		else {
			textField_additionalPoint.setText("100.0");
		}
		textField_additionalPoint.setMaximumSize(new Dimension(2147483647, 20));


		if(mCalibration.getControlPoint(1)!=null){
			spinner_AdditionalPoint.setModel(new SpinnerNumberModel(mCalibration.getControlPoint(1).getY_position(), null, null, new Integer(1)));
		}
		else{
			spinner_AdditionalPoint.setModel(new SpinnerNumberModel(mCalibration.getY(Double.valueOf(textField_additionalPoint.getText())), null, null, new Integer(1)));			
		}
		horizontalBox.add(spinner_AdditionalPoint);
		spinner_AdditionalPoint.setPreferredSize(new Dimension(50, 20));

		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
	}

	public Section getSection(){
		return pictureChoser.getSection();
	}

	public Calibration getCalibration(){
		return pictureChoser.getCalibration();
	}


	public void onChangeHappened() {
		System.out.println("Update !!!!!!!!!!!!");
		spinnerUpperPosition.setValue(mSection.getStartY());
		spinnerButtomEdge.setValue(mSection.getHight());
		spinnerLeftEdge.setValue(mSection.getStartX());
		spinnerWidth.setValue(mSection.getWidth());
	}


	@Override
	public void stateChanged(ChangeEvent arg0) { }
}
