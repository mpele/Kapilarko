package rs.mpele.capilaritty.selection;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import rs.mpele.capilaritty.Calibration;
import rs.mpele.capilaritty.Section;

import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.awt.event.ActionEvent;

public class PictureSelectionDialog extends JDialog {

	private static final long serialVersionUID = 1L;
	private PictureSelectionPanel pictureSelectionPanel;
	
	private Section mSection;
	private Calibration mCalibration;
	
	private Section mSectionOriginal;
	private Calibration mCalibrationOriginal;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Section sec = new Section(0, 1, 2, 3);
			Calibration cal = new Calibration(2, 1, 2);
			PictureSelectionDialog dialog = new PictureSelectionDialog("./Experiment1/046_2018.06.23.13.53.20.png", sec, cal);
			dialog.showDialog();
			System.out.println(sec);
			System.out.println(cal);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 * @param fileName 
	 * @param pSection 
	 */
	public PictureSelectionDialog(String fileName, Section pSection, Calibration pCalibration) {
		setTitle("Defining section and calibration");
		mSectionOriginal = pSection;
		mCalibrationOriginal = pCalibration;
		
		
		mSection = new Section(pSection);
		mCalibration = new Calibration(pCalibration);
		
		pictureSelectionPanel = new PictureSelectionPanel(fileName, mSection, mCalibration);

		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(5, 5, 650, 900);
		getContentPane().setLayout(new BorderLayout());
		pictureSelectionPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pictureSelectionPanel, BorderLayout.CENTER);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						try {
							pictureSelectionPanel.getSection().save();
							pictureSelectionPanel.getCalibration().save();
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						mSectionOriginal.setSection(pictureSelectionPanel.getSection());
						mCalibrationOriginal.setCalibration(pictureSelectionPanel.getCalibration());
						setVisible(false);
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						mSection = null;
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		pack();
	}
	
	public Section showDialog() {
	    setVisible(true);
	    return mSection;
	}
	

}
