package rs.mpele.capilaritty;

import static marvin.MarvinPluginCollection.crop;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;

public class WholePicture {

	private MarvinImage mImage;
	private Date mtime;
	
	public WholePicture(String pFile){
		mImage = MarvinImageIO.loadImage(pFile);

		Path p = Paths.get(pFile);
		String fileName = p.getFileName().toString();
		try {
			mtime = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(fileName.substring(fileName.length()-23, fileName.length()-4));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public MarvinImage getImageSection(int x, int y, int width, int high) {
		MarvinImage tmpImage = mImage.clone();
		crop(tmpImage.clone(), tmpImage, x, y, width, high);
		return tmpImage;
	}


	public PhotoSection getSection(int x, int y, int width, int high) {
		PhotoSection tmpSection = new PhotoSection();
		tmpSection.setOriginalPhoto(getImageSection(x, y, width, high));
		tmpSection.setTime(mtime);
		return tmpSection;
	}

	public PhotoSection getSection(Section section) {
		return getSection(section.getStartX(), section.getStartY(), section.getWidth(), section.getHight()) ;
	}
}
