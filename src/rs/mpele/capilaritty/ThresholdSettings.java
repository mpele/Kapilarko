package rs.mpele.capilaritty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

class ThresholdSettings {
	private Long currentStartingTimeS = 0l;
	private Long fromSecondTimeS = 0l;
	private ThresholdMethodEnum currentMethodEnum;

	private Double variable1;
	private Double variable2;
	private int min_x;
	private int max_x;

	String mFolder = ".";

	private Map<Long, ThresholdMethodEnum> mThresholdMethods = new TreeMap<Long, ThresholdMethodEnum>();
	private Map<Long, String> mThresholdParameters = new TreeMap<Long, String>();


	public static void main(String[] args) {
		try {

			//Calibration calibration = new Calibration("./sKopijaPreRucnog/Sneza3NVc/");
			Calibration calibration = new Calibration("./Proba 1 J/");
			calibration.load();


			ThresholdSettings threshordSetting = new ThresholdSettings(170, 250, 0.95, 100.);

			threshordSetting.setFolder(calibration.getFolder());
			threshordSetting.load();

			System.out.println(" ** "+threshordSetting.getThresholdMethod(10));
			System.out.println(" ** "+threshordSetting.getThresholdMethod(100));
			System.out.println(" ** "+threshordSetting.getThresholdMethod(1000));
			System.out.println(" ** "+threshordSetting.getThresholdMethod(2000));

			long time;

			System.out.println(threshordSetting);
			for(time = 0; time < 200; ){
				System.out.println(" --- "+time);
				threshordSetting.loadParameters(time);
				System.out.println(threshordSetting.getKeyForTime(time));
				threshordSetting.loadParameters(time);
				System.out.println(threshordSetting.getMin_x());
				time = time + 10;
			}

			//			ThresholdSettings copy = new ThresholdSettings(threshordSetting);
			//			System.out.println(copy);
			//			copy.setMax_x(300);
			//			copy.setThresholdMethod(ThresholdMethodEnum.Li);
			//			System.out.println(threshordSetting);
			//			System.out.println(copy);


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ThresholdSettings(int min_x, int max_x, Double variable1, Double variable2) {
		this.variable1 = variable1;
		this.variable2 = variable2;
		this.min_x = min_x;
		this.max_x = max_x;
	}

	public ThresholdSettings(ThresholdSettings old){
		variable1 = old.variable1;
		variable2 = old.variable2;
		min_x = old.min_x;
		max_x = old.max_x;

		mFolder = old.mFolder;

		for (Entry<Long, ThresholdMethodEnum> entry : old.mThresholdMethods.entrySet()) { 
			mThresholdMethods.put(entry.getKey(), ThresholdMethodEnum.valueOf(entry.getValue().name()));
		}

		for (Entry<Long, String> entry : old.mThresholdParameters.entrySet()) { 
			mThresholdParameters.put(entry.getKey(), entry.getValue());
		}
	}


	public double getVariable1() {
		return variable1;
	}

	public void setVariable1(double variable1) {
		this.variable1 = variable1;
	}

	public Double getVariable2() {
		return variable2;
	}

	public void setVariable2(Double variable2) {
		this.variable2 = variable2;
	}

	public int getMax_x() {
		return max_x;
	}

	public void setMax_x(int max_x) {
		this.max_x = max_x;
	}

	public int getMin_x() {
		return min_x;
	}

	public void setMin_x(int min_x) {
		this.min_x = min_x;
	}


	public void setFolder(String string) {
		this.mFolder = string;
	}

	public void save() throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(mFolder+"/threshold.txt")) {
			String customParameters;
			out.println("version=1.1");
			for (Long key : mThresholdMethods.keySet()) { 
				ThresholdMethodEnum method = mThresholdMethods.get(key);
				if(!key.equals(fromSecondTimeS) ){
					customParameters = "|" + mThresholdParameters.get(key);
				}
				else{
					if(method == ThresholdMethodEnum.CUSTOM){						
						mThresholdParameters.put(key, "variable1=" + variable1 + ",variable2=" + variable2 + ",min_x=" + min_x + ",max_x=" + max_x);
						customParameters = "|variable1=" + variable1 + ",variable2=" + variable2 + ",min_x=" + min_x + ",max_x=" + max_x;					
					}
					else{
						customParameters = "|" + mThresholdParameters.get(key);						
					}
				}

				out.println(key + "|" + method + customParameters);
			}


			out.close();
		}
	}	

	public void load() {
		File file = new File(mFolder+"/threshold.txt");

		try {
			BufferedReader br;
			br = new BufferedReader(new FileReader(file));
			String st = br.readLine();

			if(st.startsWith("version=1.1")){
				st = br.readLine();
				while(st != null){					
					String[] splited = st.split("\\|");

					ThresholdMethodEnum method = ThresholdMethodEnum.valueOf(splited[1]);
					mThresholdMethods.put(Long.valueOf(splited[0]), method);

					String parameters;
					if(splited.length <= 2){
						parameters = "";
					}
					else{
						parameters = splited[2];
						loadParameters(splited[2], method);
					}					
					mThresholdParameters.put(Long.valueOf(splited[0]), parameters);

					st = br.readLine();
				}
			}
			else{
				System.out.println("Old format of threshold settings");
				String[] splited = st.split(",");
				variable1 = Double.valueOf(splited[0].split("=")[1]);
				variable2 = Double.valueOf(splited[1].split("=")[1]);
				min_x = Integer.valueOf(splited[2].split("=")[1]);
				max_x = Integer.valueOf(splited[3].split("=")[1]);
				try {
					ThresholdMethodEnum thresholdMethod = ThresholdMethodEnum.valueOf(splited[4].split("=")[1]);
					Boolean bSecondThresholdMethod = Boolean.valueOf(splited[5].split("=")[1]);
					Long fromSecondThresholdMethod = Long.valueOf(splited[6].split("=")[1]);
					ThresholdMethodEnum thresholdMethodSecond = ThresholdMethodEnum.valueOf(splited[7].split("=")[1]);

					mThresholdMethods.put((long) 0, thresholdMethod);
					if(bSecondThresholdMethod){
						mThresholdMethods.put(fromSecondThresholdMethod, thresholdMethodSecond);
					}
				} catch (Exception e) {
					System.out.println("ThresholdMetod is not defined in settings.");
				}
			}

			br.close();

		} catch (IOException e) {
			System.out.println("There is no file with threshold definition. Using default fall back values...");

			//fall back value
			mThresholdMethods.put((long) 0, ThresholdMethodEnum.Otsu);
			mThresholdParameters.put((long) 0, "");
			try {
				save();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		System.out.println(this.toString());
	}

	private void loadParameters(long time, ThresholdMethodEnum thresholdMethodEnum) {
		loadParameters(mThresholdParameters.get(time), thresholdMethodEnum);

	}

	private void loadParameters(String parameters, ThresholdMethodEnum method) {
		if(method == ThresholdMethodEnum.CUSTOM){
			String[] splited2 = parameters.split(",");
			variable1 = Double.valueOf(splited2[0].split("=")[1]);
			variable2 = Double.valueOf(splited2[1].split("=")[1]);
			min_x = Integer.valueOf(splited2[2].split("=")[1]);
			max_x = Integer.valueOf(splited2[3].split("=")[1]);
		}
		else{
			variable1 = 0.;
			variable2 = 0.;
			min_x = 0;
			max_x = 255;
		}
	}


	/**
	 * return threshold method based on time
	 * @param second
	 * @return
	 */
	public ThresholdMethodEnum getThresholdMethod(long second) {

		// TODO A lot of space for optimization

		if(mThresholdMethods.size()<=1){
			return mThresholdMethods.get((long) 0); // first element
		}

		Iterator<Entry<Long, ThresholdMethodEnum>> iter = mThresholdMethods.entrySet().iterator();
		Entry<Long, ThresholdMethodEnum> current = iter.next();
		//System.out.println(current);

		for(; iter.hasNext() ;){
			Entry<Long, ThresholdMethodEnum> newThres = iter.next();
			//System.out.println(newThres + " " + newThres.getKey());
			if(newThres.getKey() > second ){
				return current.getValue();
			}
			current = newThres;
		}
		return current.getValue();
	}

	/**
	 * return threshold parameters based on time
	 * @param second
	 * @return
	 */
	public void loadThresholdParameters(long second) {
		// TODO A lot of space for optimization

		if(mThresholdMethods.size()==1){
			loadParameters((long) 0, mThresholdMethods.get((long) 0));
			return; // first element
		}

		Iterator<Entry<Long, ThresholdMethodEnum>> iter = mThresholdMethods.entrySet().iterator();
		Entry<Long, ThresholdMethodEnum> current = iter.next();
		//System.out.println(current);

		for(; iter.hasNext() ;){
			Entry<Long, ThresholdMethodEnum> newThres = iter.next();
			//System.out.println(newThres + " " + newThres.getKey());
			if(newThres.getKey() > second ){
				loadParameters(current.getKey(), current.getValue());
				return;
			}
			current = newThres;
		}
		loadParameters(current.getKey(), current.getValue());
		return;
	}


	public void setThresholdMethod(ThresholdMethodEnum thresholdMetod) {
		this.mThresholdMethods.clear();
		this.mThresholdMethods.put((long) 0, thresholdMetod);

	}

	public void setThresholdMethod(long fromSecond, ThresholdMethodEnum pThresholdMetod){
		// remove all threshold methods after newly defined method
		Iterator<Entry<Long, ThresholdMethodEnum>> it = mThresholdMethods.entrySet().iterator();
		while (it.hasNext()) {
		    Entry<Long, ThresholdMethodEnum> pair = it.next();
			if (pair.getKey() >= fromSecond) {
				mThresholdParameters.remove(pair.getKey());
				it.remove();
			}
		}
		mThresholdMethods.put(fromSecond, pThresholdMetod);
		fromSecondTimeS = fromSecond;
	}

	@Override
	public String toString() {
		return "ThresholdSettings [variable1=" + variable1 + ", variable2=" + variable2 + ", min_x=" + min_x
				+ ", max_x=" + max_x + ", mFolder=" + mFolder + ", mThresholdMethods=" + mThresholdMethods
				+ ", mThresholdParameters=" + mThresholdParameters + "]";
	}

	public void loadParameters(long time){
		currentMethodEnum = getThresholdMethod(time);
		currentStartingTimeS = getKeyForTime(time);

		loadParameters(mThresholdParameters.get(currentStartingTimeS), currentMethodEnum);
	}

	private long getKeyForTime(long time) {
		long oldKey = 0123;
		for (Long key : mThresholdMethods.keySet()) { 
			if (key > time) {
				return oldKey;
			}
			oldKey = key;
		}
		return oldKey;
	}

	public String getPosition(Long time) {
		int location = 0;
		for (Long key : mThresholdMethods.keySet()) { 
			if (key > time) {
				break;
			}
			location ++;
		}
		return location + "/" + mThresholdMethods.size();
	}
}

