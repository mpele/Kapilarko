package rs.mpele.capilaritty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;

public class Masses {

	private Map<Date, Double> mMasses = new TreeMap<Date, Double>();

	private File mFolder;

	public static void main(String[] args) throws ParseException {
		Masses masses = new Masses();
		masses.setFolder(new File("."));
		masses.load();
		System.out.println(masses);
		System.out.println(masses.getMass("2018.07.19.15.21.33"));
	}


	public void setFolder(File file) {
		mFolder = file;
	}

	public void load() {
		File file = new File(mFolder+"/mass.txt");

		mMasses.clear();

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String sr;
			while ((sr = br.readLine()) != null) {

				String[] splited = sr.split(";");
				Date time = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss.S").parse(splited[0]);
				mMasses.put(time, Double.valueOf(splited[1]));
			}

		} catch (IOException | ParseException e) {
			System.out.println("There is no file with measurements definition.");
		}


		System.out.println("Loaded:" + this.toString());
	}


	@Override
	public String toString() {
		return "Masses [mMasses=" + mMasses + ", mFolder=" + mFolder + "]";
	}

	
	/**
	 * Return value for mass for specific time recored in file
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public Double getMass(String dateString) throws ParseException {
		Date time = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(dateString);
		for (Entry<Date, Double> entry : mMasses.entrySet()) {
			if(Math.abs(entry.getKey().getTime()-time.getTime())<800){  // preciznost  +- 800 ms
				//System.out.println(entry.getKey() + ";" + entry.getValue().doubleValue());
				return entry.getValue();
			}
		}

		return 0.;
	}


	public Map<Date, Double> getWickingSet() {
		return mMasses;
	}
}
