package rs.mpele.capilaritty;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;


public class HistogramPanel extends JPanel {

	private static final long serialVersionUID = 2564616961902815084L;

	private XYSeries series;
	private Marker marker;
	private Marker additionalMarker;
	private JFreeChart chart;


	public static void main(final String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				WholePicture pic = new WholePicture("./Proba 1 J/0129_2018.08.02.18.13.37.png");

				Section section = new Section("./Proba 1 J/");
				section.load();
				PhotoSection photoSection = pic.getSection(section);

				Calibration calibration = new Calibration("./Proba 1 J/");
				calibration.load();

				photoSection.setCalibration(calibration);
				photoSection.getWicking();

				final HistogramPanel histogramPanel = new HistogramPanel("XY Series Demo 3",photoSection);
				histogramPanel.setVisible(true);

				JFrame f= new JFrame("Panel Example");
				f.add(histogramPanel);  
				f.setSize(400,400);    
				f.pack();
				f.setVisible(true);   
			}
		});
	}


	public HistogramPanel(final String title, PhotoSection photoSection) {
		IntervalXYDataset dataset = createDataset(photoSection.getHistogram());
		JFreeChart chart = createChart(dataset, photoSection.getThreshold());
		final ChartPanel chartPanel = new ChartPanel(chart);
		chartPanel.setMouseWheelEnabled(true);
		chartPanel.setPreferredSize(new java.awt.Dimension(350, 200));
		add(chartPanel);
	}


	private IntervalXYDataset createDataset(double[] histogram) {
		series = new XYSeries("histogram"); 

		for(int i = 0; i<=255; i++){
			series.add(i, histogram[i]);
		}
		final XYSeriesCollection dataset = new XYSeriesCollection(series);
		return dataset;
	}


	private JFreeChart createChart(IntervalXYDataset dataset, int threshold) {
		chart = ChartFactory.createXYBarChart(
				null,
				null, 
				false,
				null, 
				dataset,
				PlotOrientation.VERTICAL,
				false,
				false,
				false
				);
		marker = new ValueMarker(threshold);  // position is the value on the axis
		marker.setPaint(Color.BLUE);
		//marker.setLabel("here"); // see JavaDoc for labels, colors, strokes

		XYPlot plot = (XYPlot) chart.getPlot();
		plot.addDomainMarker(marker);

		return chart;    
	}

	public void setData(PhotoSection photoSection) {
		series.clear();			
		//series = new XYSeries("histogram");


		for(int i = 0; i<=255; i++){
			series.add(i, photoSection.getHistogram()[i]);
		}
		((ValueMarker) marker).setValue(photoSection.getThreshold());
	}

	public void setAdditionalMarker(int value){	
		if(additionalMarker == null){
			additionalMarker = new ValueMarker(value);  // position is the value on the axis
			additionalMarker.setPaint(Color.BLACK);
			//marker.setLabel("here"); // see JavaDoc for labels, colors, strokes

			XYPlot plot = (XYPlot) chart.getPlot();
			plot.addDomainMarker(additionalMarker);
		}
		else{
			((ValueMarker) additionalMarker).setValue(value);
		}

	}
}
