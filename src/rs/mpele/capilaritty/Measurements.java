package rs.mpele.capilaritty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.Set;

public class Measurements {

	private Map<Long, Double> mWicking = new TreeMap<Long, Double>();
	private Map<Long, Boolean> mWickingManual = new TreeMap<Long, Boolean>();
	private Map<Long, String> mFile = new TreeMap<Long, String>();

	private String mFolder = ".";
	private Date mReferentTime;


	public Measurements(File folder2) {
		mFolder = folder2.getPath();
	}

	public Double getWicking(Long time) {
		return mWicking.get(time);
	}

	public String getFile(Long time) {
		return mFile.get(time);
	}

	public void setFile(Long time, String file) {
		this.mFile.put(time, file);
	}

	public String getDateString(Long time) {
		return mFile.get(time).substring(5, 24);
	}
	/**
	 * wickingManual is true for manual set
	 * @param time
	 * @param wicking
	 * @param wickingManual
	 */
	public void setWicking(Long time, Double wicking, Boolean wickingManual) {
		this.mWicking.put(time, wicking);
		this.mWickingManual.put(time, wickingManual);				
	}

	public Boolean getWickingManual(Long time) {
		Boolean tmp = mWickingManual.get(time);
		if(tmp == null){
			return false;
		}
		else{
			return tmp;
		}
	}

	public void setWickingManual(Long time, Boolean wickingManual) {
		this.mWickingManual.put(time, wickingManual);
	}

	public void save() throws FileNotFoundException{
		try (PrintWriter out = new PrintWriter(mFolder+"/measurements.txt")) {
			for (Entry<Long, Double> entry : mWicking.entrySet()) {
				out.println(entry.getKey() + ";" + entry.getValue().doubleValue() 
						+ ";" + mWickingManual.get(entry.getKey())
						+ ";" + mFile.get(entry.getKey()));
			}
		}
	}

	public void load() {
		File file = new File(mFolder+"/measurements.txt");

		mWicking.clear();
		mWickingManual.clear();

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String sr;
			while ((sr = br.readLine()) != null) {

				String[] splited = sr.split(";");
				Long time = Long.valueOf(splited[0]);
				mWicking.put(time, Double.valueOf(splited[1]));
				mWickingManual.put(time, Boolean.valueOf(splited[2]));
				mFile.put(time, splited[3]);

				if(mReferentTime==null){
					mReferentTime = Measurements.getTimeFromFilename(splited[3]);
				}
			}

			br.close();
		} catch (IOException e) {
			System.out.println("There is no file with measurements definition.");
		}


		System.out.println("Loaded: "+this.toString());
	}

	public Set<Long> getTimes() {
		return mWicking.keySet();
	}

	public Map<Long, Double> getWickingSet(){
		return mWicking;
	}

	@Override
	public String toString() {
		return "Measurements [mWicking=" + mWicking + ", mWickingManual=" + mWickingManual + ", mFile=" + mFile
				+ ", mFolder=" + mFolder + "]";
	}

	public String getFolder() {
		return mFolder;
	}

	public void export2csv() throws FileNotFoundException {
		//		Locale fmtLocale = Locale.getDefault(Category.FORMAT);
		//		NumberFormat formatter = NumberFormat.getInstance(fmtLocale);
		//		formatter.setMaximumFractionDigits(1);
		//		formatter.setMinimumFractionDigits(1);
		DecimalFormat formatter = (DecimalFormat) DecimalFormat.getInstance();
		formatter.applyPattern("#####.#");
		try (PrintWriter out = new PrintWriter(mFolder+"/measurements.csv")) {
			for (Entry<Long, Double> entry : mWicking.entrySet()) {
				out.println(entry.getKey() + ";" + formatter.format(entry.getValue().doubleValue()) );
			}
		}
	}

	public void setReferentTime(Date pReferentTime) {
		mReferentTime = pReferentTime;
	}

	public Date getReferentTime(){
		if(mReferentTime != null){
			return mReferentTime;
		}
		else{
			return new Date();
		}
	}

	static public Date getTimeFromFilename(String fileName){
		try {
			return new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(fileName.substring(fileName.length()-23, fileName.length()-4));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static long secondsOfExperiment(Date time, Date pRreferentTime) {
		return (time.getTime()-pRreferentTime.getTime())/1000;
	}

	public void removeAllExceptManualySetted() {
		ArrayList<Long> list = new ArrayList<Long>();  

		for (Entry<Long, Boolean> entry : mWickingManual.entrySet()) {
			if(entry.getValue().booleanValue()==false){
				list.add(entry.getKey());
			}
		}		
		for(Long s : list){			
			mWicking.remove(s);
			mFile.remove(s);
			mWickingManual.remove(s);
		}
	}

	public void deleteMeasurement(Long time) {
		mWicking.remove(time);
		mWickingManual.remove(time);
		mFile.remove(time);		
	}
	
	public boolean isThereAnyManualySettedWicking(){
		for (Entry<Long, Boolean> entry : mWickingManual.entrySet()) {
			if(entry.getValue().booleanValue()==true){
				return true;
			}
		}
		return false;
	}
}
