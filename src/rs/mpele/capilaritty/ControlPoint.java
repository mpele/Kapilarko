package rs.mpele.capilaritty;

public class ControlPoint{
	boolean active;
	int y_position;
	double value;
	

	public ControlPoint(boolean active, int y_position, double value) {
		super();
		this.active = active;
		this.y_position = y_position;
		this.value = value;
	}
	
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}


	public int getY_position() {
		return y_position;
	}

	public void setY_position(int y_position) {
		this.y_position = y_position;
	}
	
	
	public Double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}