package rs.mpele.capilaritty;
import org.apache.poi.ss.usermodel.Chart;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.charts.AxisCrosses;
import org.apache.poi.ss.usermodel.charts.AxisPosition;
import org.apache.poi.ss.usermodel.charts.ChartDataSource;
import org.apache.poi.ss.usermodel.charts.ChartLegend;
import org.apache.poi.ss.usermodel.charts.DataSources;
import org.apache.poi.ss.usermodel.charts.LegendPosition;
import org.apache.poi.ss.usermodel.charts.ScatterChartData;
import org.apache.poi.ss.usermodel.charts.ScatterChartSeries;
import org.apache.poi.ss.usermodel.charts.ValueAxis;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

public class ExportToExcel {

	XSSFWorkbook workbook = new XSSFWorkbook();
	XSSFSheet sheet = workbook.createSheet("Measurements");

	int colNum = 0;
	private ScatterChartData data;

	public static void main(String[] args) throws IOException {

		String file = "Measurements.xlsx";

		String mainFolder = "H:\\Jovanina merenja\\SredjenoDragana";
		//String mainFolder = "H:\\Jovanina merenja\\SredjenoSneza";
		//String mainFolder = "Sredjeno";

		ExportToExcel exportToExcel = new ExportToExcel(ExportType.WICKING, mainFolder);
		//ExportToExcel exportToExcel = new ExportToExcel(ExportType.WICKIN_MASS);
		//ExportToExcel exportToExcel = new ExportToExcel(ExportType.MASS);
		exportToExcel.close(file);


		Desktop desktop = Desktop.getDesktop();
		desktop.open(new File(file));
	}

	public ExportToExcel(ExportType exportTypeEnum, String pMainFolder) throws IOException {

		File folder = new File(pMainFolder);
		File[] listOfFiles = folder.listFiles();
		Arrays.sort(listOfFiles);

		@SuppressWarnings("rawtypes")
		Drawing drawing = sheet.createDrawingPatriarch();
		ClientAnchor anchor = drawing.createAnchor(0, 0, 0, 0, 0, 5, 20, 30);

		Chart chart = drawing.createChart(anchor);
		ChartLegend legend = chart.getOrCreateLegend();
		legend.setPosition(LegendPosition.TOP_RIGHT);

		data = chart.getChartDataFactory()
				.createScatterChartData();

		ValueAxis bottomAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.BOTTOM);
		ValueAxis leftAxis = chart.getChartAxisFactory().createValueAxis(AxisPosition.LEFT);
		leftAxis.setCrosses(AxisCrosses.AUTO_ZERO);



		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isDirectory()) { 
				String folderName = pMainFolder+"/"+listOfFiles[i].getName();
				File tmp = new File(folderName + "/measurements.txt");
				if(tmp.exists()){
					try {
						if(exportTypeEnum == ExportType.WICKIN_MASS){
							processSampleWickingMass(folderName);
						}
						else if(exportTypeEnum == ExportType.MASS){
							processSampleMass(folderName);
						}
						else if(exportTypeEnum == ExportType.WICKING){
							processSampleWickig(folderName);
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else{
					System.out.println("Folder " + folderName + " doesn't have measurements");
				}
			}
		}


		chart.plot(data, bottomAxis, leftAxis);
	}

	public void processSampleWickingMass(String folderName) throws ParseException {
		System.out.println("Processing "+folderName);
		Measurements measurements = new Measurements(new File(folderName));
		measurements.load();
		Map<Long, Double> wicking = measurements.getWickingSet();

		Masses mass = new Masses();
		mass.setFolder(new File(folderName));
		mass.load();

		int rowNum = 0;
		String sampleName = new File(folderName).getName();

		// Header
		Row row = getRow(rowNum++);
		row.createCell(colNum).setCellValue(sampleName);
		row = getRow(rowNum++);
		row.createCell(colNum).setCellValue((String) "Time, s");
		row.createCell(colNum + 1).setCellValue((String) "Wicking, mm");
		row.createCell(colNum + 2).setCellValue((String) "Mass, g");

		//data
		for (Entry<Long, Double> entry : wicking.entrySet()) {
			Row rowData;
			rowNum ++;
			rowData = getRow(rowNum);
			rowData.createCell(colNum).setCellValue((Long) entry.getKey());
			rowData.createCell(colNum + 1).setCellValue((Double) entry.getValue().doubleValue());
			rowData.createCell(colNum + 2).setCellValue((Double) mass.getMass(measurements.getDateString((Long) entry.getKey())));
		}

		//graph
		ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(3, rowNum, colNum, colNum));
		ChartDataSource<Number> ys = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(3, rowNum, colNum+1, colNum+1));

		ScatterChartSeries chartSeries = data.addSerie(xs, ys);
		chartSeries.setTitle(sampleName);

		colNum = colNum + 3;
	}

	public void processSampleWickig(String folderName) throws ParseException {
		System.out.println("Processing "+folderName);
		Measurements measurements = new Measurements(new File(folderName));
		measurements.load();

		Map<Long, Double> wicking = measurements.getWickingSet();

		int rowNum = 0;
		String sampleName = new File(folderName).getName();

		// Header
		Row row = getRow(rowNum++);
		row.createCell(colNum + 1).setCellValue(sampleName);
		row = getRow(rowNum++);
		row.createCell(colNum).setCellValue((String) "Time, min");
		row.createCell(colNum + 1).setCellValue((String) "Wicking, mm");

		//data
		for (Entry<Long, Double> entry : wicking.entrySet()) {
			Row rowData;
			rowNum ++;
			rowData = getRow(rowNum);
			rowData.createCell(colNum).setCellValue((Long) entry.getKey()/60.);
			rowData.createCell(colNum + 1).setCellValue((Double) entry.getValue().doubleValue());
		}

		//graph
		ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(3, rowNum, colNum, colNum));
		ChartDataSource<Number> ys = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(3, rowNum, colNum+1, colNum+1));

		ScatterChartSeries chartSeries = data.addSerie(xs, ys);
		chartSeries.setTitle(sampleName);

		colNum = colNum + 2;
	}

	public void processSampleMass(String folderName) throws ParseException {
		System.out.println("Processing "+folderName);
		Measurements measurements = new Measurements(new File(folderName));
		measurements.load();
		Date referentTime = measurements.getReferentTime();

		Masses mass = new Masses();
		mass.setFolder(new File(folderName));
		mass.load();
		Map<Date, Double> massSet = mass.getWickingSet();


		int rowNum = 0;
		String sampleName = new File(folderName).getName();

		// Header
		Row row = getRow(rowNum++);
		row.createCell(colNum).setCellValue(sampleName);
		row = getRow(rowNum++);
		row.createCell(colNum).setCellValue((String) "Time, s");
		row.createCell(colNum + 1).setCellValue((String) "Mass, g");

		//data
		for (Entry<Date, Double> entry : massSet.entrySet()) {
			long time = (long)(((Date) entry.getKey()).getTime()-referentTime.getTime())/1000;
			if(time<0){
				continue;
			}
			Row rowData;
			rowNum ++;
			rowData = getRow(rowNum);
			rowData.createCell(colNum).setCellValue(time);
			rowData.createCell(colNum + 1).setCellValue((Double) entry.getValue().doubleValue());
		}

		//graph
		ChartDataSource<Number> xs = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(3, rowNum, colNum, colNum));
		ChartDataSource<Number> ys = DataSources.fromNumericCellRange(sheet, new CellRangeAddress(3, rowNum, colNum+1, colNum+1));

		ScatterChartSeries chartSeries = data.addSerie(xs, ys);
		chartSeries.setTitle(sampleName);

		colNum = colNum + 3;
	}


	private Row getRow(int rowNum) {
		Row rowData;
		rowData = sheet.getRow(rowNum);
		if(rowData == null){
			rowData = sheet.createRow(rowNum);
		}
		return rowData;
	}

	public void close(String fileName) {
		try {
			FileOutputStream outputStream = new FileOutputStream(fileName);
			workbook.write(outputStream);
			workbook.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Done");

	}
}

enum ExportType {
	WICKIN_MASS,
	MASS, WICKING
}