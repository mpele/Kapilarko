package rs.mpele.capilaritty;

enum ThresholdMethodEnum {
	CUSTOM,
	Default,
	Huang,
	Intermodes,
	IsoData,
	IJ_IsoData,
	Li,
	MaxEntropy,
	Mean,
	MinError,
	Minimum,
	Moments,
	Otsu,
	Percentile,
	RenyiEntropy,
	Shanbhag,
	Triangle,
	Yen
} 