package rs.mpele.capilaritty.neuroph;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset; 
import org.jfree.data.xy.XYSeries;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.neuroph.core.NeuralNetwork;
import rs.mpele.capilaritty.Measurements;
import rs.mpele.capilaritty.WickingSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Set;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;

public class GraphPanelForNeuroph extends ApplicationFrame {

	private static final long serialVersionUID = 1L;
	private XYSeriesCollection dataset;
	private JFreeChart xylineChart;
	private XYSeries neurophWickingData;
	private ChartPanel chartPanel;


	public static void main( String[ ] args ) throws IOException {
		GraphPanelForNeuroph graphPanelForNeuroph = new GraphPanelForNeuroph("-", "?");

		String folder = "H:\\testiranjeNeuroph\\testiranje\\";
		//String folder = "H:\\testiranjeNeuroph\\BAMCoBa\\";
		//String folder = "H:\\testiranjeNeuroph\\BAMCoBb\\";
		//String folder = "H:\\testiranjeNeuroph\\BAMCoBc\\";


		NeuralNetwork<?> loadedMlPerceptron = NeuralNetwork.createFromFile("myMlPerceptron.nnet");

		WickingSet wicking = null;
		try {
			wicking = new WickingSet(new File(folder));
			wicking.processFiles(true);			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		graphPanelForNeuroph.setAndSaveToTxtFile(wicking, loadedMlPerceptron, "");

		//		graphPanelForNeuroph.pack();
		//System.out.println("pre");
		//		graphPanelForNeuroph.createImage("fileName.png");
		//		System.out.println("posle");

		RefineryUtilities.centerFrameOnScreen( graphPanelForNeuroph );          
		graphPanelForNeuroph.setVisible( true );


	}

	public GraphPanelForNeuroph( String applicationTitle, String chartTitle ) {
		super(applicationTitle);
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setBounds(10, 10, 800, 600);
		setTitle(applicationTitle);

		xylineChart = ChartFactory.createXYLineChart(
				chartTitle ,
				"Time" ,
				"Wicking" ,
				createDataset() ,
				PlotOrientation.VERTICAL ,
				true , true , false);

		chartPanel = new ChartPanel( xylineChart );
		chartPanel.setMouseWheelEnabled(true);

		chartPanel.setPreferredSize( new java.awt.Dimension( 800 , 600 ) );
		//		final XYPlot plot = xylineChart.getXYPlot( );

		//		XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer( );
		//		renderer.setSeriesPaint( 0 , Color.RED );
		//		renderer.setSeriesPaint( 1 , Color.GREEN );
		//		renderer.setSeriesPaint( 2 , Color.YELLOW );
		//		renderer.setSeriesStroke( 0 , new BasicStroke( 4.0f ) );
		//		renderer.setSeriesStroke( 1 , new BasicStroke( 3.0f ) );
		//		renderer.setSeriesStroke( 2 , new BasicStroke( 2.0f ) );
		//		plot.setRenderer( renderer ); 

		new XYSeries("Wicking, mm");

		neurophWickingData = new XYSeries("neuroph");
		neurophWickingData.clear();

		((XYSeriesCollection) dataset).addSeries(neurophWickingData);

		setContentPane( chartPanel ); 
	}

	/**
	 * Saves data to txt file if the filename is longer than 4 characters
	 * @param wicking
	 * @param neurophPerceptron
	 * @param fileName
	 * @return
	 */
	
	double setAndSaveToTxtFile(WickingSet wicking, NeuralNetwork<?> neurophPerceptron, String fileName) {
		setMeasurements(wicking.getMeasurements());

		Set<Long> keyset = wicking.getMeasurements().getWickingSet().keySet();
		String forFile = "";

		double sumR = 0;

		for( Long key : keyset){
			int neurophTreshold = wicking.getPhotoSectionMap().get(key).getThreshold(neurophPerceptron);
			double neurophValue = wicking.getPhotoSectionMap().get(key).readWicking(neurophTreshold, true);
			//System.out.println(key + " "+ neurophTreshold + " " + value);
			addNeurophPoint(key, neurophValue);

			double originalValue = wicking.getMeasurements().getWicking(key);
			sumR += Math.pow((originalValue - neurophValue), 2);
			forFile = forFile + ";" + key + ";" + originalValue + ";" + neurophValue + "\n";
		}

		if(fileName.length()>4){
			try (PrintWriter out = new PrintWriter(new FileWriter(fileName,false))) {
				out.println(forFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		double mse = sumR/wicking.getMeasurements().getWickingSet().size();
		xylineChart.setTitle(xylineChart.getTitle().getText()+" MSE:" + mse);
		
		return mse;
	}

	private void addNeurophPoint(Long time, double value) {
		neurophWickingData.add(time / 60., value);		
	}

	private XYDataset createDataset( ) {
		dataset = new XYSeriesCollection( );          
		return dataset;
	}

	protected void setMeasurements(Measurements measurement) {
		Set<Long> times = measurement.getTimes();
		XYSeries newWickingData = new XYSeries(measurement.getFolder());
		newWickingData.clear();

		for (Long time : times) {
			newWickingData.add(time / 60., measurement.getWicking(time));
		}
		((XYSeriesCollection) dataset).addSeries(newWickingData);
	}

	void createImage(String fileName)  {
		// TODO snimanje u fajl je previse sporo !!!!

		//		OutputStream out = new FileOutputStream(fileName);
		//		ChartUtilities.writeChartAsPNG(out,
		//				xylineChart,
		//				chartPanel.getWidth(),
		//				chartPanel.getHeight());

		try {
			File f = new File(fileName);
			if(!f.exists()) 
				f.createNewFile();

			ChartUtilities.saveChartAsJPEG(f, xylineChart, 800, 400);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}