package rs.mpele.capilaritty.neuroph;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TransferFunctionType;


public class Proba {

	public static void main(String[] args) {
		
		DataSet treningSet = loadDataSetFromFile();
		// create training set (logical XOR function)
		DataSet trainingSet = new DataSet(2, 1);
		trainingSet.addRow(new DataSetRow(new double[]{0, 0}, new double[]{0}));
		trainingSet.addRow(new DataSetRow(new double[]{0, 1}, new double[]{1}));
		trainingSet.addRow(new DataSetRow(new double[]{1, 0}, new double[]{1}));
		trainingSet.addRow(new DataSetRow(new double[]{1, 1}, new double[]{0}));
		
		// trainingSet is a DataSet object from an other class
		BackPropagation train = new BackPropagation();
        final double rate = 0.02;
        final double maxError = 0.0001;
        train.setLearningRate(rate);
        train.setMaxError(maxError);
		
		// create multi layer perceptron
		MultiLayerPerceptron myMlPerceptron = new MultiLayerPerceptron(TransferFunctionType.TANH, 2, 3, 1);
		myMlPerceptron.setLearningRule(train);
		
		System.out.println("Training neural network");
//		// learn the training set
//		Thread t = new Thread() {
//            public void run() {
                long t = System.currentTimeMillis();
                myMlPerceptron.learn(trainingSet);
                t = System.currentTimeMillis()-t;
                System.out.println("Time : " + t + " ms. Iterations: "+train.getCurrentIteration());
//            }
//        };
//        t.start();

        //---------------------------------------------
        
		// test perceptron
		System.out.println("Testing trained neural network ");
		testNeuralNetwork(myMlPerceptron, trainingSet);

		// save trained neural network
		myMlPerceptron.save("myMlPerceptron123.nnet");

		
		//---------------------------------------------

//		// load saved neural network
		NeuralNetwork loadedMlPerceptron = NeuralNetwork.createFromFile("myMlPerceptron123.nnet");
//
//		// test loaded neural network
		System.out.println("Testing loaded neural network");
		testNeuralNetwork(loadedMlPerceptron, trainingSet);

	}

	public static void testNeuralNetwork(NeuralNetwork nnet, DataSet testSet) {

		for(DataSetRow dataRow : testSet.getRows()) {
			nnet.setInput(dataRow.getInput());
			nnet.calculate();
			double[ ] networkOutput = nnet.getOutput();
			System.out.print("Input: " + Arrays.toString(dataRow.getInput()) );
			System.out.println(" Output: " + Arrays.toString(networkOutput) );
		}

	}
	
	public static DataSet loadDataSetFromFile(){
		File file = new File("exportForNeuroph.txt");

		DataSet dataSet = new DataSet(106,1);
		
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String sr;
			while ((sr = br.readLine()) != null) {

				String[] splited = sr.split(",");
				double dataDouble[] = new double[splited.length-1];
				
				for(int i = 0; i < splited.length-2; i++){
					dataDouble[i++] = Double.valueOf(splited[i]);
				}
				
				double resultData[] = new double[]{Double.valueOf(splited[splited.length-1])};
				System.out.println(dataDouble.length);
				dataSet.add(new DataSetRow(dataDouble, resultData));
			}

		} catch (IOException e) {
			System.out.println("Problem with reading data for Neuroph.");
		}
		return null;
	}
	
}