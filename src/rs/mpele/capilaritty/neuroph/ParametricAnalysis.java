package rs.mpele.capilaritty.neuroph;

import org.jfree.ui.ApplicationFrame;
import org.neuroph.core.data.DataSet;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.util.TransferFunctionType;

import rs.mpele.capilaritty.Beep;
import rs.mpele.capilaritty.WickingSet;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class ParametricAnalysis extends ApplicationFrame {

	private static final long serialVersionUID = 1L;

	private DataSet dataSetTraning;
	List<WickingSet> wickingSetList;
	DecimalFormat df = new DecimalFormat("#");

	int counter = 0;
	int counterANN = 0;

	public static void main(String[] args) throws IOException {
		ParametricAnalysis parametricAnalysis = new ParametricAnalysis("");

		List<String> listSamples = new ArrayList<>();
		//listSamples.add("H:\\testiranjeNeuroph\\testiranje\\");
		//listSamples.add("H:\\testiranjeNeuroph\\DraganaCoOa_L\\");
		listSamples.add("H:\\testiranjeNeuroph\\D_CoOb\\");
		listSamples.add("H:\\testiranjeNeuroph\\DraganaCoOa\\");
		//listSamples.add("H:\\testiranjeNeuroph\\BAMCoBa\\");
		//listSamples.add("H:\\testiranjeNeuroph\\BAMCoBb\\");
		//listSamples.add("H:\\testiranjeNeuroph\\BAMCoBc\\");

		parametricAnalysis.prepeareSamplesForGraph(listSamples);


		//parametricAnalysis.prepeareTrainingDataSet("H:\\testiranjeNeuroph\\testiranje\\");
		//parametricAnalysis.prepeareTrainingDataSet("H:\\testiranjeNeuroph\\D_CoOb\\");
		//parametricAnalysis.prepeareTrainingDataSet("H:\\testiranjeNeuroph\\DraganaCoOa\\");
		//parametricAnalysis.prepeareTrainingDataSet("H:\\testiranjeNeuroph\\DraganaCoOa_L\\", false);
		parametricAnalysis.prepeareTrainingDataSet("H:\\testiranjeNeuroph\\DraganaCoOa_L2\\", true);
		//parametricAnalysis.prepeareTrainingDataSet(listSamples.get(0));


		List<Double> maxErrorList = new ArrayList<>();
		maxErrorList.add(0.001);
		//maxErrorList.add(0.0004);
		//maxErrorList.add(0.0003);
		//maxErrorList.add(0.0001);

		List<TransferFunctionType> transferFunctionList = new ArrayList<>();
		transferFunctionList.add(TransferFunctionType.SIGMOID);
		//transferFunctionList.add(TransferFunctionType.GAUSSIAN);

		List<Integer> neuronsInLayers = new ArrayList<>();
		neuronsInLayers.add(106);
		neuronsInLayers.add(60);
		neuronsInLayers.add(1);

		int repetitions = 3;

		for(int i = 0; i <= 2 ; i++){
			for(int j = 0; j <= 3 && j <= i; j++){
				for(int k = 0; k <= 1 && k <= j; k++){
					neuronsInLayers = new ArrayList<>();
					neuronsInLayers.add(106);
					neuronsInLayers.add(20 + i * 20);
					if(j!=0){						
						neuronsInLayers.add(j*20);
					}
					if(k!=0) {
						neuronsInLayers.add(k*10);
					}
					neuronsInLayers.add(1);

					parametricAnalysis.process(repetitions, maxErrorList, neuronsInLayers, transferFunctionList);			
				}	
			}
		}

		System.out.println("Gotovo");
		Beep.end();
	}



	public ParametricAnalysis(String title) {
		super(title);
		df.setMaximumFractionDigits(8);
	}



	public void prepeareTrainingDataSet(String samplePath, boolean exportDataForNeuroph) {
		if(exportDataForNeuroph)
			Utils.exportDataForNeuroph(samplePath);

		dataSetTraning = DataSet.createFromFile(samplePath+"exportForNeuroph.txt", 106, 1, ",");
		// TODO Mozda koristiti subsets
	}



	private void prepeareSamplesForGraph(List<String> listSamples) {
		wickingSetList = new ArrayList<>();

		for (int i = 0; i < listSamples.size(); i++) {
			try {
				WickingSet wicking = null;
				wicking = new WickingSet(new File(listSamples.get(i)));
				wicking.processFiles(true);
				wicking.ExportForNeuroph(150, 255);
				wickingSetList.add(wicking);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}

	public void process(int repetitions, List<Double> maxErrorList, List<Integer> neuronsInLayers, List<TransferFunctionType> transferFunctionList){
		BackPropagation train = new BackPropagation();
		final double rate = 0.02;
		double maxError = 0.0001;
		int maxIterations = 300000;

		for(int i =0; i < 1; i++){
			for(int curMaxError = 0; curMaxError < maxErrorList.size(); curMaxError ++){
				for(int curTransferFunction= 0; curTransferFunction < transferFunctionList.size(); curTransferFunction ++){
					maxError = maxErrorList.get(curMaxError);
					List<List<Double>> oneSettings = new ArrayList<>();
					for(int rep = 1; rep <= repetitions ; rep++){
						List<Double> results = new ArrayList<>();
						oneSettings.add(results);
						// trainingSet is a DataSet object from an other class
						train.setLearningRate(rate);
						train.setMaxError(maxError);
						train.setMaxIterations(maxIterations);

						// create multi layer perceptron				
						MultiLayerPerceptron myMlPerceptron = new MultiLayerPerceptron(neuronsInLayers, transferFunctionList.get(curTransferFunction));

						//MultiLayerPerceptron myMlPerceptron = new MultiLayerPerceptron(TransferFunctionType.TANH, 106, 90, 80, 60, 1);
						myMlPerceptron.setLearningRule(train);


						printResults("----------------------------------------");
						printResults(counterANN + " Training neural network " + neuronsInLayers + " MaxError=" + df.format(maxError) + " " + rep);

						long t = System.currentTimeMillis();
						myMlPerceptron.learn(dataSetTraning); 
						//myMlPerceptron.learn(dataSetTest);
						t = System.currentTimeMillis()-t;
						printResults("Time : " + t + " ms. Iterations: " + train.getCurrentIteration() + " MaxError="+df.format(maxError) + " TotalNetworkError=" + df.format(train.getTotalNetworkError()));

						if(maxIterations <= train.getCurrentIteration()){
							Beep.longBeep();
						}
						
						myMlPerceptron.save("ANN_"+counterANN+"_"+counter+".nnet");
						counterANN++;

						for(int numDataset = 0; numDataset < wickingSetList.size(); numDataset++){
							WickingSet wicking = wickingSetList.get(numDataset);
							GraphPanelForNeuroph graphPanelForNeuroph = new GraphPanelForNeuroph(
									wicking.getFolder()+ "\n"+
											neuronsInLayers +" MaxError=" + df.format(maxError), 

											counterANN + "_" + counter + "\n" +
											wicking.getFolder()+ " Rep:" + rep + "\n"+
													"Training data:" + dataSetTraning.getFilePath()+ "\n"+
													neuronsInLayers +" MaxError=" + df.format(maxError) + " " +transferFunctionList.get(curTransferFunction));

							double varTmp = graphPanelForNeuroph.setAndSaveToTxtFile(wicking, myMlPerceptron, "ANN_"+counter+".txt");
							results.add(varTmp);
							System.out.println(""+varTmp);
							graphPanelForNeuroph.pack();
							graphPanelForNeuroph.setVisible(true);	

							try {
								Thread.sleep(700);
								graphPanelForNeuroph.createImage("ANN_"+counter+".jpg");
								counter++;
								Thread.sleep(500);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}
					}
					printResults(oneSettings);
				}
			}
		}
	}

	private void printResults(String text){
		System.out.println(text);
		try (PrintWriter out = new PrintWriter(new FileWriter("ANN_results.txt",true))) {
			out.println(text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printResults(List<List<Double>> results) {
		String tmp = "";
		//header
		for(int i = 0; i< results.get(0).size(); i++){
			tmp += wickingSetList.get(i).getSampleName() + " ";			
		}
		printResults(tmp);

		// data
		for(int j = 0; j< results.size(); j++){
			tmp = "";
			for(int i = 0; i< results.get(j).size(); i++){
				tmp += results.get(j).get(i) + " ";
			}
			printResults(tmp);
		}
	}
}
