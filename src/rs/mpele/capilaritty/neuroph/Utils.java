package rs.mpele.capilaritty.neuroph;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;

import rs.mpele.capilaritty.Beep;
import rs.mpele.capilaritty.WickingSet;

public class Utils {

	public static void main(String[] args) {
//		System.out.println(Utils.getThreshold(100, 255, Utils.getValue(100, 255, 190)));
		Utils.exportDataForNeuroph("H:\\testiranjeNeuroph\\BAMCoBa\\");
		Utils.exportDataForNeuroph("H:\\testiranjeNeuroph\\BAMCoBb\\");
		Utils.exportDataForNeuroph("H:\\testiranjeNeuroph\\BAMCoBc\\");
		Utils.exportDataForNeuroph("H:\\testiranjeNeuroph\\testiranje\\");
		//System.out.println(Utils.getThreshold(100, 255, 0.7032258064516129));

		Beep.end();
	} 

	public static int getThreshold(int min, int max, double value){
		return (int) Math.round(min+(max-min)*value);
	}

	public static double getValue(int min, int max, int threshold){
		return (threshold-min)/Double.valueOf(max-min);
	}
	
	public static void exportDataForNeuroph(String filename){

		WickingSet wicking;
		try {
			wicking = new WickingSet(new File(filename));
			wicking.processFiles(true);
			wicking.ExportForNeuroph(150, 255);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
