package rs.mpele.capilaritty.neuroph;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.neuroph.core.NeuralNetwork;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.util.TransferFunctionType;


public class TestsWithRealData {
	public static void main(String[] args) {
		//DataSet trainingSet = loadDataSetFromFile();
		//DataSet trainingSet = DataSet.createFromFile("exportForNeuroph.txt", 106, 1, ",");
		DataSet dataSet1 = DataSet.createFromFile("H:\\testiranjeNeuroph\\BAMCoBa\\exportForNeuroph.txt", 106, 1, ",");
		DataSet dataSet2 = DataSet.createFromFile("H:\\testiranjeNeuroph\\BAMCoBc\\exportForNeuroph.txt", 106, 1, ",");
		DataSet dataSet3 = DataSet.createFromFile("H:\\testiranjeNeuroph\\BAMCoBb\\exportForNeuroph.txt", 106, 1, ",");

		DataSet dataSetTest = DataSet.createFromFile("H:\\testiranjeNeuroph\\testiranje\\exportForNeuroph.txt", 106, 1, ",");

		dataSet1.addAll(dataSet2);
		dataSet1.addAll(dataSet3);
		
		//		Normalizer norm = new MaxMinNormalizer();
		//		norm.normalize(dataSet1);
		//		norm.normalize(dataSet2);
		//		norm.normalize(dataSet3);

		DataSet[] trainingSubSet = dataSet1.createTrainingAndTestSubsets(100, 80);

		// trainingSet is a DataSet object from an other class
		BackPropagation train = new BackPropagation();
		final double rate = 0.02;
		final double maxError = 0.0001;
		train.setLearningRate(rate);
		train.setMaxError(maxError);
		train.setMaxIterations(300000);

		// create multi layer perceptron
		MultiLayerPerceptron myMlPerceptron = new MultiLayerPerceptron(TransferFunctionType.TANH, 106, 60, 1);
		//MultiLayerPerceptron myMlPerceptron = new MultiLayerPerceptron(TransferFunctionType.TANH, 106, 90, 80, 60, 1);
		myMlPerceptron.setLearningRule(train);

		System.out.println("Training neural network");
		//		// learn the training set
		//		Thread t = new Thread() {
		//            public void run() {
		long t = System.currentTimeMillis();
		myMlPerceptron.learn(trainingSubSet[0]);
		//myMlPerceptron.learn(dataSetTest);
		t = System.currentTimeMillis()-t;
		System.out.println("Time : " + t + " ms. Iterations: "+train.getCurrentIteration());
		//            }
		//        };
		//        t.start();

		//---------------------------------------------

		// test perceptron
		System.out.println("Testing trained neural network ");
		//testNeuralNetwork(myMlPerceptron, trainingSet);

		// save trained neural network
		myMlPerceptron.save("myMlPerceptron.nnet");


		System.out.println("---------------------- trening" + dataSet1.getFilePath());
		testNeuralNetwork(myMlPerceptron, trainingSubSet[0]);
		System.out.println("---------------------- " + dataSet1.getFilePath());
		testNeuralNetwork(myMlPerceptron, dataSet1);
		System.out.println("---------------------- " + dataSet2.getFilePath());
		testNeuralNetwork(myMlPerceptron, dataSet2);
		System.out.println("---------------------- " + dataSet3.getFilePath());
		testNeuralNetwork(myMlPerceptron, dataSet3);

		System.out.println("---------------------- " + dataSetTest.getFilePath());
		testNeuralNetwork(myMlPerceptron, dataSetTest);


		//---------------------------------------------

		// load saved neural network
		NeuralNetwork loadedMlPerceptron = NeuralNetwork.createFromFile("myMlPerceptron.nnet");
		//
		//		// test loaded neural network
		System.out.println("Testing loaded neural network");
		//				testNeuralNetwork(loadedMlPerceptron, dataSet1.createTrainingAndTestSubsets(0, 30)[1]);
		testNeuralNetwork(loadedMlPerceptron, dataSetTest);
		System.out.println("Gotovo");
	}

	public static void testNeuralNetwork(NeuralNetwork nnet, DataSet testSet) {

		for(DataSetRow dataRow : testSet.getRows()) {
			nnet.setInput(dataRow.getInput());
			nnet.calculate();
			double[ ] networkOutput = nnet.getOutput();
			//System.out.print("Input: " + Arrays.toString(dataRow.getInput()) );
			System.out.println("Expected " + Utils.getThreshold(100, 255, dataRow.getDesiredOutput()[0])
			+ " Output: " + Utils.getThreshold(100, 255, networkOutput[0]) 
			+ " Error: " + (Utils.getThreshold(100, 255, dataRow.getDesiredOutput()[0])
					-Utils.getThreshold(100, 255, networkOutput[0])));
		}

	}

	public static DataSet loadDataSetFromFile(){
		File file = new File("exportForNeuroph.txt");

		DataSet dataSet = new DataSet(106,1);

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String sr;
			while ((sr = br.readLine()) != null) {

				String[] splited = sr.split(",");
				double dataDouble[] = new double[splited.length-1];

				for(int i = 0; i < splited.length-2; i++){
					dataDouble[i++] = Double.valueOf(splited[i]);
				}

				double resultData[] = new double[]{Double.valueOf(splited[splited.length-1])};
				dataSet.add(new DataSetRow(dataDouble, resultData));
			}

		} catch (IOException e) {
			System.out.println("Problem with reading data for Neuroph.");
		}
		System.out.println("Loaded data...");
		return dataSet;
	}

}