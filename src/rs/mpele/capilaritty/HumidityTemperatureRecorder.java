package rs.mpele.capilaritty;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import com.fazecast.jSerialComm.SerialPort;

public class HumidityTemperatureRecorder extends ApplicationFrame {

	private static final long serialVersionUID = 1L;
	private TimeSeries temperatureData;
	private TimeSeries humidityData;
	private TimeSeriesCollection humidityDataset;
	private SerialPort comPort;
	private Double mTemperature;
	private Double mHumidity;
	private ChartPanel chartPanel;

	String filename= "humidityTemperature.txt";
	private Thread threadRecording;



	public static void main(final String[] args) throws InterruptedException {

		final HumidityTemperatureRecorder demo = new 
				HumidityTemperatureRecorder("Humidity and Temperature Recorder");
		demo.pack();
		RefineryUtilities.centerFrameOnScreen(demo);
		demo.setVisible(true);

	}

	public HumidityTemperatureRecorder(final String title) throws InterruptedException {
		super(title);

		initialize();

		this.temperatureData = new TimeSeries("Temperature");
		final TimeSeriesCollection dataset = new TimeSeriesCollection(this.temperatureData);
		final JFreeChart chart = createChart(dataset);


		final JPanel content = new JPanel(new BorderLayout());

		chartPanel = new ChartPanel(chart);
		chartPanel.setMouseWheelEnabled(true);
		content.add(chartPanel);

		chartPanel.setPreferredSize(new java.awt.Dimension(800, 270));
		setContentPane(content);

		loadOldData();

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				try {
					startRecording();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		threadRecording = new Thread(runnable);
		Thread.sleep(3000);

		threadRecording.start();
	}

	private JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart result = ChartFactory.
				createTimeSeriesChart(null, "Time", "Temperature, C", dataset,
						true, true, false);
		final XYPlot plot = result.getXYPlot();
		ValueAxis axis = plot.getDomainAxis();
		//axis.setAutoRange(true);
		axis = plot.getRangeAxis();
		axis.setFixedAutoRange(3*60*60*1000.0); // 3 hours
		axis.setRange(20, 35);

		//for second axis
		humidityData = new TimeSeries("Humidity");		
		humidityDataset = new TimeSeriesCollection(humidityData);

		plot.setDataset(1, humidityDataset);
		plot.mapDatasetToRangeAxis(1, 1);

		XYItemRenderer renderer2 = new StandardXYItemRenderer();
		renderer2.setSeriesPaint(0, Color.blue);
		plot.setRenderer(1, renderer2);

		final ValueAxis axis2 = new NumberAxis("Humidity, %");
		axis2.setRange(40, 65);
		plot.setRangeAxis(1, axis2);
		plot.setRangeAxisLocation(1, AxisLocation.BOTTOM_OR_RIGHT);
		return result;
	}


	public void initialize() {
		SerialPort[] ports = SerialPort.getCommPorts();

		for(SerialPort port: ports){
			System.out.println(port.getDescriptivePortName());
			if(port.getDescriptivePortName().contains("CH340")){				
				comPort = port;
			}			
		}
		
		if(comPort == null){
			JOptionPane.showMessageDialog(this,
					"Arduino is not connected !!!",
					"Inane error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		System.out.println("Connecting to: "+comPort.getDescriptivePortName());
		boolean isOpen = comPort.openPort();
		//TODO !!! napraviti proveru
//		if(!comPort.isOpen()) {
//			JOptionPane.showMessageDialog(this,
//					"Port is not opened !!!",
//					"Inane error",
//					JOptionPane.ERROR_MESSAGE);
//		}
		comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 1000, 0);
		clearBuffer();
	}

	public void clearBuffer(){
		byte[] readBuffer2 = new byte[1000]; // number of bytes that are expected				
		comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 10, 0);
		while(comPort.bytesAvailable()>0){
			comPort.readBytes(readBuffer2, readBuffer2.length);
		}
	}


	private void startRecording() throws InterruptedException {
		while(true){
			readHumidityTemperature();
			temperatureData.add(new Millisecond(), mTemperature);
			humidityData.add(new Millisecond(), mHumidity);
			threadRecording.sleep(60000);
		}
	}

	private void readHumidityTemperature() {

		if(!comPort.isOpen()) {
			JOptionPane.showMessageDialog(null,
					"Port for Humidity Temperature is not open!",
					"Inane error",
					JOptionPane.ERROR_MESSAGE);
			System.out.println("!!!!!!!!!!!!! port is not opened");
		}

		clearBuffer();

		sendCommand("GET\n");

		boolean haveResult = false;
		String data = "";
		byte[] readBuffer = new byte[50]; // number of bytes that are expected 
		comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, 2000, 0);
		int counter = 0;

		while(haveResult == false){ // TODO !!!!! set timer
			int numRead = comPort.readBytes(readBuffer, readBuffer.length);
			if(numRead>0){
				data = data + new String(readBuffer);				
			}
			//System.out.println("Read " + numRead + " bytes: " + data);

			String[] tmp = data.split("\n");
			if(tmp.length> 1 && tmp[counter].startsWith("Temperature")){
				readData(tmp[counter]);
				haveResult = true;
			}
			else{
				counter++;
			}
		}

		appendToFile();	
	}

	private Double readData(String string) {
		mTemperature = Double.valueOf(string.substring(13, 19));
		mHumidity = Double.valueOf(string.substring(33, 39));
		System.out.println(mTemperature+" "+mHumidity);
		return null;
	}

	private void sendCommand(String command) {
		byte[] arduinoKomanda = (command).getBytes();
		comPort.writeBytes(arduinoKomanda , arduinoKomanda.length);
	}

	private void appendToFile() {
		try
		{
			FileWriter fw = new FileWriter(filename,true); //the true will append the new data
			fw.write(new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date()) + ";" + String.valueOf(mHumidity)+";" + String.valueOf(mTemperature)+"\n");
			fw.close();
		}
		catch(IOException ioe)
		{
			// TODO
			System.err.println("IOException: " + ioe.getMessage());
		}		
	}


	private void loadOldData(){
		File file = new File(filename);
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(file));

			String sr;
			while ((sr = br.readLine()) != null) {

				String[] splited = sr.split(";");
				Date time = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").parse(splited[0]);
				temperatureData.add(new Millisecond(time), Double.valueOf(splited[2]));
				humidityData.add(new Millisecond(time), Double.valueOf(splited[1]));
			}

		} catch (IOException | ParseException e) {
			System.out.println("There is no file with measurements definition.");
		}
	}
}
