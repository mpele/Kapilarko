package rs.mpele.capilaritty;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import rs.mpele.capilaritty.neuroph.Utils;

public class WickingSet {

	private Map<Long, PhotoSection> mSectionMap = new TreeMap<Long, PhotoSection>();
	private File mFolder;
	private Section mSection = new Section();
	private Calibration mCalibration = new Calibration();
	private Measurements measurements;
	private Date mReferentTime;
	private ThresholdSettings mThresholdSettings = new ThresholdSettings(170, 250, 0.9, 100.);;


	public static void main(String[] args) {
		try {

			Section section = new Section("./Proba 1 J/");
			section.load();

			Calibration calibration = new Calibration("./Proba 1 J/");
			calibration.load();

			long startTime = System.currentTimeMillis();

			WickingSet wicking = new WickingSet(new File("./Proba 1 J/"), 
					section, calibration);
			wicking.processFiles(true);

			
			wicking.ExportForNeuroph(150, 255);

			long stopTime = System.currentTimeMillis();
			long elapsedTime = stopTime - startTime;
			System.out.println("Execution time:"+elapsedTime/1000);

		} catch (ParseException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public WickingSet(File workFolder) throws FileNotFoundException, ParseException {
		mFolder = workFolder;

		mCalibration.setFolder(mFolder);
		mCalibration.load();

		mSection.setFolder(mFolder);
		mSection.load();

		mThresholdSettings.setFolder(mFolder.getPath());
		mThresholdSettings.load();

		loadMeasurementsData();
	}

	public WickingSet(File pFolder, Section section, Calibration calibration) throws ParseException, FileNotFoundException {
		mFolder = pFolder;
		mCalibration= calibration;
		mSection = section;

		loadMeasurementsData();
	}



	public void loadMeasurementsData() {
		mReferentTime = new Date();  //reset referent time - it will be changed if there are measurements

		measurements = new Measurements(mFolder);
		measurements.load();

		for(Long a : measurements.getTimes()){
			mSectionMap.put(a, null);
		}
	}

	/**
	 * Processing all png files in working folder
	 * @throws ParseException
	 * @throws FileNotFoundException
	 */
	public void processFiles(boolean processAllFiles) throws ParseException, FileNotFoundException{
		if(processAllFiles == true){
			measurements = new Measurements(mFolder);
			mSectionMap.clear();
		}
		else{
			measurements.removeAllExceptManualySetted();
		}

		// TODO !!! ovako se ne uklanjanju rucno zadate vrednosti, ali nisam siguran da li bi trebalo - proveriti

		File folder = mFolder;
		File[] listOfFiles = folder.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".png");
			}
		});
		Arrays.sort(listOfFiles);

		if(listOfFiles.length>0){
			mReferentTime = Measurements.getTimeFromFilename(listOfFiles[0].getName());
		}
		else{
			mReferentTime = new Date();
		}

		measurements.setReferentTime(mReferentTime);

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile() && listOfFiles[i].getName().contains("png")) { 
				addFile(listOfFiles[i]);
			}
		}

		System.out.println(measurements);
		measurements.save();
	}

	public void addFile(File pFile) throws FileNotFoundException {
		System.out.println("Add file " + pFile.getName());
		WholePicture pic = new WholePicture(mFolder+"/"+pFile.getName());
		PhotoSection photoSection = pic.getSection(mSection.getStartX(), mSection.getStartY(), mSection.getWidth(), mSection.getHight());
		photoSection.setCalibration(mCalibration);
		photoSection.setThresholdSettings(mThresholdSettings);

		if(mReferentTime == null){
			mReferentTime = new Date();
		}

		photoSection.setReferentTime(mReferentTime);
		System.out.println(photoSection.getSecondOfExperiment() + " - " + photoSection.readWicking());

		mSectionMap.put(photoSection.getSecondOfExperiment(), photoSection);

		if(!measurements.getWickingManual(photoSection.getSecondOfExperiment())){
			measurements.setWicking(photoSection.getSecondOfExperiment(), photoSection.getWicking(), false);
		}
		measurements.setFile(photoSection.getSecondOfExperiment(), pFile.getName());
		measurements.save();
	}



	public Map<Long, PhotoSection> getPhotoSectionMap() {
		return mSectionMap;
	}

	void setFolder(File file) throws FileNotFoundException, ParseException {
		mFolder = file;
		loadMeasurementsData();
	}

	public Measurements getMeasurements() {
		return measurements;
	}

	public int getPosLastSlide() {
		if(mSectionMap.size()==0){
			return 0;
		}
		else{
			return mSectionMap.size()-1;
		}
	}

	public Section getSection() {
		return mSection;
	}

	public void setSection(Section mSection) {
		this.mSection = mSection;
	}

	public Calibration getCalibration() {
		return mCalibration;
	}

	public void setCalibration(Calibration mCalibration) {
		this.mCalibration = mCalibration;
	}

	public void setThresholdSettings(ThresholdSettings res) {
		mThresholdSettings = res;
	}


	public void updateThresholdSettings() {		
		for (Entry<Long, PhotoSection> entry : mSectionMap.entrySet())
		{
			Long time = entry.getKey();

			if(!measurements.getWickingManual(time)){
				PhotoSection tmpPhotoSection = entry.getValue();
				tmpPhotoSection.setThresholdSettings(mThresholdSettings);
				System.out.println(tmpPhotoSection.readWicking() + " "+ tmpPhotoSection.getThreshold());

				measurements.setWicking(tmpPhotoSection.getSecondOfExperiment(), tmpPhotoSection.getWicking(), false);
			}else{
				System.out.println("Wicking setted maualy");
			}
		}
		try {
			measurements.save();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteMeasurement(Long time) throws FileNotFoundException {
		mSectionMap.remove(time);
		measurements.deleteMeasurement(time);
		measurements.save();
	}

	public String getFolder() {
		return mFolder.getAbsolutePath();
	}

	public String getSampleName() {
		return mFolder.getName();
	}
	
	public ThresholdSettings getThresholdSettings() {
		return mThresholdSettings;
	}

	
	public void ExportForNeuroph(int min, int max) {
	System.out.println("Export for Neuroph: " + min +"-" + max);
	try (PrintWriter out = new PrintWriter(mFolder+"/exportForNeuroph.txt")) {
		for (Entry<Long, PhotoSection> entry : mSectionMap.entrySet())
		{
			String output = "";
			PhotoSection tmpPhotoSection = entry.getValue();

			double[] tmpHistogram = tmpPhotoSection.getHistogram();
			
			for(int i = min; i <= max ; i++){
				output += tmpHistogram[i] + ", ";
			}
			output = output + Utils.getValue(150, 255, tmpPhotoSection.getThreshold());
			//System.out.println(output);
			out.println(output);
		}
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
	System.out.println("Export for Neuroph done.");
}
}
