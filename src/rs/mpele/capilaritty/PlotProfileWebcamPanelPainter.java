package rs.mpele.capilaritty;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.github.sarxos.webcam.WebcamPanel;

public class PlotProfileWebcamPanelPainter implements WebcamPanel.Painter{


	public void paintImage(WebcamPanel panel, BufferedImage image, Graphics2D g2) {
		Graphics2D g = image.createGraphics();

		int centralLine = image.getHeight()/2;

		Color rgb;
		Color oldRgb = new Color(0, 0, 0);
		int oldGray = 0;
		for(int i = 10; i < image.getWidth(); i++){
			rgb = new Color(image.getRGB(i, centralLine));
			g.setColor(Color.RED);
			g.drawLine(i-1, centralLine - (oldRgb.getRed()-128)*2, i, centralLine - (rgb.getRed()-128)*2);
			g.setColor(Color.GREEN);
			g.drawLine(i-1, centralLine - (oldRgb.getGreen()-128)*2, i, centralLine - (rgb.getGreen()-128)*2);
			//g.drawLine(i-1, centralLine - oldRgb.getGreen(), i, centralLine - rgb.getGreen());
			g.setColor(Color.BLUE);
			g.drawLine(i-1, centralLine - (oldRgb.getBlue()-128)*2, i, centralLine - (rgb.getBlue()-128)*2);
			g.setColor(Color.GRAY);
			int gray = (int) (0.2126 * oldRgb.getRed() + 0.7152 * oldRgb.getGreen()+ 0.0722 *oldRgb.getBlue());
			g.drawLine(i-1, centralLine - (oldGray-128)*2, i, centralLine - (gray-128)*2);
			oldRgb = rgb;
			oldGray = gray;
		}

		g.setColor(Color.WHITE);
		g.drawLine(0, centralLine, image.getWidth(), centralLine);
		g.setColor(Color.lightGray);
		g.drawLine(0, centralLine-127, image.getWidth(), centralLine-127);
		g.drawLine(0, centralLine-255, image.getWidth(), centralLine-255);

		int fontSize = 90;

		g.setFont(new Font("TimesRoman", Font.PLAIN, fontSize));


		g.setColor(Color.WHITE);
		g.drawString("Ura !!! " , 300, 300);

		g.dispose();

		panel.getDefaultPainter().paintImage(panel, image, g2);
	}


	public void paintPanel(WebcamPanel panel, Graphics2D g2) {
		panel.getDefaultPainter().paintPanel(panel, g2);
	}

}