package rs.mpele.capilaritty;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Section {

	private int startX = 0;
	private int startY = 0;
	private int width = 0;
	private int high = 0;

	String mFolder = ".";


	public Section(int startX, int startY, int width, int hight) {
		super();
		this.startX = startX;
		this.startY = startY;
		this.width = width;
		this.high = hight;
	}

	public Section() {

	}

	public Section(String folder) {
		mFolder = folder;	
	}

	public Section(Section section) {
		startX = section.getStartX();
		startY = section.getStartY();
		width = section.getWidth();
		high = section.getHight();
		mFolder = section.getFolder();
	}
	
	private String getFolder() {
		return mFolder;
	}

	public void setSection(Section section) {
		startX = section.getStartX();
		startY = section.getStartY();
		width = section.getWidth();
		high = section.getHight();
		mFolder = section.getFolder();
	}

	public void setFolder(File folder2) {
		this.mFolder = folder2.getPath();
	}

	public int getStartX() {
		return startX;
	}
	public void setStartX(int startX) {
		this.startX = startX;
	}
	public int getStartY() {
		return startY;
	}
	public void setStartY(int startY) {
		this.startY = startY;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	// hight width
	public int getHight() {
		return high;
	}
	public void setHight(int high) {
		this.high = high;
	}

	@Override
	public String toString() {
		return "Section [startX=" + startX + ", startY=" + startY + ", width=" + width + ", high=" + high + "]";
	}


	public void save() throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(mFolder+"/section.txt")) {
			out.println("startX=" + startX + ",startY=" + startY + ",width=" + width + ",high=" + high);
			out.close();
		}
	}	

	public void load() {
		File file = new File(mFolder+"/section.txt");

		try {
			BufferedReader br;
			br = new BufferedReader(new FileReader(file));
			String st = br.readLine();

			String[] splited = st.split(",");
			startX = Integer.valueOf(splited[0].split("=")[1]);
			startY = Integer.valueOf(splited[1].split("=")[1]);
			width = Integer.valueOf(splited[2].split("=")[1]);
			high = Integer.valueOf(splited[3].split("=")[1]);

			br.close();

		} catch (IOException e) {
			System.out.println("There is no file with section definition.");
		}


		System.out.println(this.toString());
	}

}
