import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

import rs.mpele.capilaritty.Balance;

public class TestVage extends ApplicationFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	/** The time series data. */
	private TimeSeries series;
	private Balance balance;


	public TestVage(final String title) {
		super(title);


		balance = new Balance();
		balance.setWorkFolder(new File("."));
		balance.initialize();


		this.series = new TimeSeries("Masa");
		final TimeSeriesCollection dataset = new TimeSeriesCollection(this.series);
		final JFreeChart chart = createChart(dataset);

		final ChartPanel chartPanel = new ChartPanel(chart);
		final JButton button = new JButton("Add New Data Item");
		button.setActionCommand("ADD_DATA");
		button.addActionListener(this);

		final JPanel content = new JPanel(new BorderLayout());
		content.add(chartPanel);
		content.add(button, BorderLayout.SOUTH);
		chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		setContentPane(content);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						while(true) {
							series.add(new Millisecond(), balance.readMass());
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								throw new IllegalStateException(e);
							}
						}
					}
				};

				Thread thread = new Thread(runnable);

				thread.start();
			}
		});
		content.add(btnNewButton, BorderLayout.NORTH);

	}

	private JFreeChart createChart(final XYDataset dataset) {
		final JFreeChart result = ChartFactory.
				createTimeSeriesChart("Dynamic Data Demo", "Time", "Value", dataset,
						true, true, false);
		final XYPlot plot = result.getXYPlot();
		ValueAxis axis = plot.getDomainAxis();
		axis.setAutoRange(true);
		axis.setFixedAutoRange(600000.0); // 600 seconds
		axis = plot.getRangeAxis();
		axis.setRange(0.0, 5.0);
		return result;
	}

	public void actionPerformed(final ActionEvent e) {
		if (e.getActionCommand().equals("ADD_DATA")) {
			final Millisecond now = new Millisecond();
			System.out.println("Now = " + now.toString());
			this.series.add(new Millisecond(), balance.readMass());
		}
	}

	public static void main(final String[] args) {

		final TestVage demo = new 
				TestVage("Dynamic Data Demo");
		demo.pack();
		RefineryUtilities.centerFrameOnScreen(demo);
		demo.setVisible(true);

	}
}


// // Arduino code for testing

//#include <CmdParser.hpp>
//
//CmdParser cmdParser;
//
//double rezultat = 5;
//boolean stalnoStampa  = true;
//
//void setup()
//{
//  pinMode(13, OUTPUT);
//
//  // Open serial communications and wait for port to open:
//  Serial.begin(9600);
//  while (!Serial) {
//    ; // wait for serial port to connect. Needed for native USB port only
//  }
//}
//
//void loop()
//{
//  if (Serial.available()) {
//    // use own buffer from serial input
//    CmdBuffer<300> myBuffer;
//
//    // Read line from Serial until timeout
//    if (myBuffer.readFromSerial(&Serial, 30000)) {
//      if (cmdParser.parseCmd(&myBuffer) != CMDPARSER_ERROR) {
//        if (cmdParser.equalCmdParam(0, "SI")) {
//          delay(3000);
//          stampajVrednost();
//        }
//        if (cmdParser.equalCmdParam(0, "C1")) {
//          stalnoStampa = true;
//        }
//      }
//    }
//  }
//  if (stalnoStampa == true) {
//    stampajVrednost();
//    delay(100);
//  }
//
//}
//
//void stampajVrednost() {
//  rezultat = rezultat * (0.9 + 0.2 * (random(100) / 100.));
//  char outstr[15];
//  dtostrf(rezultat, 9, 4, outstr);
//
//  Serial.print(" g  \n\r");
//  Serial.print("SI   -");
//  Serial.print(outstr);
//  Serial.print(" g  \n\r");
//}