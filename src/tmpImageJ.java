import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.io.Opener;

public class tmpImageJ {

	public static void main(String[] args) {
		ImagePlus image = new Opener().openImage("sKopijaPreRucnog/Sneza3NVc/0006_2018.08.27.07.48.25.png");
		IJ.run(image, "8-bit", "");
		
		//IJ.run(image, "HSB Stack", "");
		//image.show();
		//image.setSlice(3);
		
		IJ.setAutoThreshold(image, "RenyiEntropy dark");
//		IJ.setAutoThreshold(image, "Shanbhag dark");
//		IJ.setAutoThreshold(image, "Triangle dark");
//		IJ.setAutoThreshold(image, "Yen dark");
//		IJ.setAutoThreshold(image, "Triangle dark");
//		IJ.setAutoThreshold(image, "Shanbhag dark");
//		IJ.setAutoThreshold(image, "RenyiEntropy dark");
//		IJ.setAutoThreshold(image, "Percentile dark");
//		IJ.setAutoThreshold(image, "Otsu dark");
//		IJ.setAutoThreshold(image, "Moments dark");
//		IJ.setAutoThreshold(image, "Minimum dark");
//		IJ.setAutoThreshold(image, "MinError dark");

		System.out.println(image.getProcessor().getAutoThreshold());
		System.out.println(image.getProcessor().getMinThreshold());
		System.out.println(image.getProcessor().getMaxThreshold());
		
		Prefs.blackBackground = true;
		IJ.run(image, "Convert to Mask", "only");
		image.show();
	}
}
